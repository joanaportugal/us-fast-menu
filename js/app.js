import ProductsView from "./views/ProductsView.js";
import RestaurantsView from "./views/RestaurantsView.js";

class App {
  constructor() {
    this.routes = {
      "": [],
      index: [RestaurantsView],
      subway: [RestaurantsView, ProductsView],
      starbucks: [RestaurantsView, ProductsView],
      mcdonalds: [RestaurantsView, ProductsView],
      dunkin: [RestaurantsView, ProductsView],
      burgerking: [RestaurantsView, ProductsView],
      wendys: [RestaurantsView, ProductsView],
      tacobell: [RestaurantsView, ProductsView],
      kfc: [RestaurantsView, ProductsView],
    };

    // instantiate the views mapped in the routes object
    this.#instantiateViews();
  }

  #instantiateViews() {
    const path = window.location.pathname;
    const file = path.substr(path.lastIndexOf("/") + 1);
    const route = file.split(".")[0];
    const views = this.#getViews(route);
    for (const view of views) {
      new view();
    }
  }

  #getViews(route) {
    return typeof this.routes[route] === "undefined" ? [] : this.routes[route];
  }
}

new App();
