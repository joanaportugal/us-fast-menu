const card = (category, product) => {
  if (
    ["frappuccino", "coldCoffees", "icedTeas", "coldDrinks"].some(
      (item) => item == category
    )
  ) {
    return product.price
      ? `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price: ${product.price} US$</p>
        </div>
      </div>`
      : `
    <div class="card col-12 col-md-2 my-3 m-md-3">
      <img class="card-img-top" src="${product.img}" alt="${product.name}">
      <div class="card-body">
        <h5 class="card-title">${product.name}</h5>
      </div>
      <div class="card-footer">
      <p class="card-text">Price (Tall): ${product.priceTall} US$</p>
      <p class="card-text">Price (Grande): ${product.priceGrande} US$</p>
      <p class="card-text">Price (Venti): ${product.priceVenti} US$</p>
      </div>
    </div>`;
  } else {
    return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price: ${product.price} US$</p>
        </div>
      </div>`;
  }
};

export default card;
