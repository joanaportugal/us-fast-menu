const card = (category, product) => {
  if (["sandwiches", "freshMelts"].some((item) => item == category)) {
    return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price (6 Inch Regular Sub): 
        ${product.price6Inch} US$</p>
        <p class="card-text">Price (Footlong Regular Sub): 
        ${product.priceFootlong} US$</p>
        ${
          product.priceFootlongPro
            ? `<p class="card-text">
              Price (Footlong Pro): ${product.priceFootlongPro} US$
              </p>`
            : ""
        }
        </div>
      </div>`;
  } else {
    return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price: ${product.price} US$</p>
        </div>
      </div>`;
  }
};

export default card;
