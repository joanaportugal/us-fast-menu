const card = (category, product) => {
  return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price: ${product.price} US$</p>
        ${
          product.priceLarge
            ? `<p class="card-text">Price (Large): ${product.priceLarge} US$</p>`
            : ""
        }
        </div>
      </div>`;
};

export default card;
