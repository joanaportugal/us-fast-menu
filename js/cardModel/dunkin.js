const card = (category, product) => {
  if (
    ["sandwichesWraps", "donutsBagels", "bakerySnacks"].some(
      (item) => item == category
    )
  ) {
    return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price: ${product.price} US$</p>
        </div>
      </div>`;
  } else {
    return `
      <div class="card col-12 col-md-2 my-3 m-md-3">
        <img class="card-img-top" src="${product.img}" alt="${product.name}">
        <div class="card-body">
          <h5 class="card-title">${product.name}</h5>
        </div>
        <div class="card-footer">
        <p class="card-text">Price (Small): ${product.priceSmall} US$</p>
        <p class="card-text">Price (Medium): ${product.priceMedium} US$</p>
        <p class="card-text">Price (Large): ${product.priceLarge} US$</p>
        ${
          product.priceXLarge
            ? `<p class="card-text">Price (X-Large): ${product.priceXLarge} US$</p>`
            : ""
        }
        </div>
      </div>`;
  }
};

export default card;
