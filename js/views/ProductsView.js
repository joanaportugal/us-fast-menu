import CategoriesController from "../controllers/CategoriesController.js";
import ProductsController from "../controllers/ProductsController.js";
import CardSubway from "../cardModel/subway.js";
import CardStarbucks from "../cardModel/starbucks.js";
import CardMcDonalds from "../cardModel/mcdonalds.js";
import CardDunkin from "../cardModel/dunkin.js";
import CardBurgerKing from "../cardModel/burgerKing.js";
import CardWendys from "../cardModel/wendys.js";
import CardTacoBell from "../cardModel/tacoBell.js";
import CardKFC from "../cardModel/kfc.js";

export default class CategoriesView {
  constructor() {
    this.categoriesController = new CategoriesController();
    this.productsController = new ProductsController();
    this.selectBurgerKing = document.getElementById("selectBurgerKing");
    this.selectDunkin = document.getElementById("selectDunkin");
    this.selectKFC = document.getElementById("selectKFC");
    this.selectMcDonalds = document.getElementById("selectMcDonalds");
    this.selectStarbucks = document.getElementById("selectStarbucks");
    this.selectSubway = document.getElementById("selectSubway");
    this.selectTacoBell = document.getElementById("selectTacoBell");
    this.selectWendys = document.getElementById("selectWendys");
    this.products = document.getElementById("products");

    if (this.selectBurgerKing) this.categoriesBurgerKing();
    if (this.selectDunkin) this.categoriesDunkin();
    if (this.selectKFC) this.categoriesKFC();
    if (this.selectMcDonalds) this.categoriesMcDonalds();
    if (this.selectStarbucks) this.categoriesStarbucks();
    if (this.selectSubway) this.categoriesSubway();
    if (this.selectTacoBell) this.categoriesTacoBell();
    if (this.selectWendys) this.categoriesWendys();
  }

  categoriesBurgerKing() {
    const categories = this.categoriesController.categoriesBurgerKing();

    for (const category of categories) {
      this.selectBurgerKing.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectBurgerKing.addEventListener("change", () => {
      const list = this.productsController.productsBurgerKing(
        this.selectBurgerKing.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardBurgerKing(
          this.selectBurgerKing.value,
          item
        );
      }
    });
  }

  categoriesDunkin() {
    const categories = this.categoriesController.categoriesDunkin();
    for (const category of categories) {
      this.selectDunkin.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectDunkin.addEventListener("change", () => {
      const list = this.productsController.productsDunkin(
        this.selectDunkin.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardDunkin(this.selectDunkin.value, item);
      }
    });
  }

  categoriesKFC() {
    const categories = this.categoriesController.categoriesKFC();
    for (const category of categories) {
      this.selectKFC.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectKFC.addEventListener("change", () => {
      const list = this.productsController.productsKFC(this.selectKFC.value);
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardKFC(this.selectKFC.value, item);
      }
    });
  }

  categoriesMcDonalds() {
    const categories = this.categoriesController.categoriesMcDonalds();
    for (const category of categories) {
      this.selectMcDonalds.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectMcDonalds.addEventListener("change", () => {
      const list = this.productsController.productsMcDonalds(
        this.selectMcDonalds.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardMcDonalds(
          this.selectMcDonalds.value,
          item
        );
      }
    });
  }

  categoriesStarbucks() {
    const categories = this.categoriesController.categoriesStarbucks();
    for (const category of categories) {
      this.selectStarbucks.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectStarbucks.addEventListener("change", () => {
      const list = this.productsController.productsStarbucks(
        this.selectStarbucks.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardStarbucks(
          this.selectStarbucks.value,
          item
        );
      }
    });
  }

  categoriesSubway() {
    const categories = this.categoriesController.categoriesSubway();

    for (const category of categories) {
      this.selectSubway.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }
    this.selectSubway.addEventListener("change", () => {
      const list = this.productsController.productsSubway(
        this.selectSubway.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardSubway(this.selectSubway.value, item);
      }
    });
  }

  categoriesTacoBell() {
    const categories = this.categoriesController.categoriesTacoBell();
    for (const category of categories) {
      this.selectTacoBell.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectTacoBell.addEventListener("change", () => {
      const list = this.productsController.productsTacoBell(
        this.selectTacoBell.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardTacoBell(
          this.selectTacoBell.value,
          item
        );
      }
    });
  }

  categoriesWendys() {
    const categories = this.categoriesController.categoriesWendys();
    for (const category of categories) {
      this.selectWendys.innerHTML += `<option value="${category.val}">${category.txt}</option>`;
    }

    this.selectWendys.addEventListener("change", () => {
      const list = this.productsController.productsWendys(
        this.selectWendys.value
      );
      this.products.innerHTML = "";

      for (const item of list) {
        this.products.innerHTML += CardWendys(this.selectWendys.value, item);
      }
    });
  }
}
