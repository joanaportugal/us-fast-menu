import RestaurantsController from "../controllers/RestaurantsController.js";

export default class RestaurantsView {
  constructor() {
    this.restaurantsController = new RestaurantsController();
    this.restaurantsList = document.getElementById("restaurantsList");
    this.restaurantInfo = document.getElementById("restaurantInfo");
    if (this.restaurantsList) {
      this.listRestaurants();
    }
    if (this.restaurantInfo) {
      this.getRestaurantInfo();
    }
  }

  getImgSize(rName) {
    switch (rName) {
      case "Wendy's":
        return 160;
      case "McDonald's":
        return 175;
      case "Dunkin'":
        return 270;
      default:
        return 150;
    }
  }

  listRestaurants() {
    const list = this.restaurantsController.getList();
    for (const item of list) {
      this.restaurantsList.innerHTML += `
      <article class="d-flex flex-column align-items-center col-4 col-md-2 mx-4 mx-md-2">
      <img src="${item.logo}" alt="${item.name}" width="${this.getImgSize(
        item.name
      )}"/>
      <p class="py-2">${item.name}</p>
      </article>
      `;
    }
  }

  getName(route) {
    switch (route) {
      case "burgerking":
        return "Burger King";
      case "dunkin":
        return "Dunkin'";
      case "kfc":
        return "KFC";
      case "mcdonalds":
        return "McDonald's";
      case "starbucks":
        return "Starbucks";
      case "subway":
        return "Subway";
      case "tacobell":
        return "Taco Bell";
      case "wendys":
        return "Wendy's";
      default:
        return "";
    }
  }

  getRestaurantInfo() {
    const path = window.location.pathname;
    const file = path.substr(path.lastIndexOf("/") + 1);
    const route = file.split(".")[0];
    const info = this.restaurantsController.getRestaurant(this.getName(route));
    this.restaurantInfo.innerHTML = `
    <img src=".${info.logo}" alt="${info.name}" width="${
      this.getImgSize(info.name) / 1.5
    }" />
    <h2 class="px-2">${info.name}</h2>
    `;
  }
}
