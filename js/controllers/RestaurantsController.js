export default class ListController {
  constructor() {
    this.restaurants = [
      { name: "Subway", logo: "./img/subway.png" },
      { name: "Starbucks", logo: "./img/starbucks.png" },
      { name: "McDonald's", logo: "./img/mcdonalds.png" },
      { name: "Dunkin'", logo: "./img/dunkin.png" },
      { name: "Burger King", logo: "./img/burgerking.png" },
      { name: "Wendy's", logo: "./img/wendys.png" },
      { name: "Taco Bell", logo: "./img/tacobell.png" },
      { name: "KFC", logo: "./img/kfc.png" },
    ];
  }

  getList() {
    return this.restaurants;
  }

  getRestaurant(rName) {
    return this.restaurants.find(({ name }) => name === rName);
  }
}
