import BurgerKing from "../products/burgerking.js";
import Dunkin from "../products/dunkin.js";
import KFC from "../products/kfc.js";
import McDonalds from "../products/mcdonalds.js";
import Starbucks from "../products/starbucks.js";
import Subway from "../products/subway.js";
import TacoBell from "../products/tacobell.js";
import Wendys from "../products/wendys.js";

export default class ProductsController {
  constructor() {}

  productsBurgerKing(category) {
    return BurgerKing[category];
  }

  productsDunkin(category) {
    return Dunkin[category];
  }

  productsKFC(category) {
    return KFC[category];
  }

  productsMcDonalds(category) {
    return McDonalds[category];
  }

  productsStarbucks(category) {
    return Starbucks[category];
  }

  productsSubway(category) {
    return Subway[category];
  }

  productsTacoBell(category) {
    return TacoBell[category];
  }

  productsWendys(category) {
    return Wendys[category];
  }
}
