export default class CategoriesController {
  constructor() {
    this.currDate = new Date();
  }

  categoriesBurgerKing() {
    const morningCategories = [
      { val: "bfMeals", txt: "Breakfast Meals" },
      { val: "bfEntrees", txt: "Breakfast Entrees" },
      { val: "bfBeverages", txt: "Breakfast Beverages" },
      { val: "bfSides", txt: "Breakfast Sides" },
    ];

    const afternoonCategories = [
      { val: "familyBundles", txt: "Family Bundles" },
      { val: "meals", txt: "Meals" },
      { val: "aLaCarte", txt: "A La Carte" },
      { val: "beverages", txt: "Beverages" },
      { val: "sides", txt: "Sides" },
      { val: "desserts", txt: "Desserts" },
      { val: "kingJrMeals", txt: "King Jr. Meals" },
    ];

    return this.currDate.getHours() >= 6 && this.currDate.getHours() < 11
      ? morningCategories
      : afternoonCategories;
  }

  categoriesDunkin() {
    const categories = [
      { val: "espressoDrinks", txt: "Espresso Drinks" },
      { val: "icedDrinks", txt: "Iced Drinks" },
      { val: "hotDrinks", txt: "Hot Drinks" },
      { val: "frozenDrinks", txt: "Frozen Drinks" },
      { val: "sandwichesWraps", txt: "Sandwiches & Wraps" },
      { val: "donutsBagels", txt: "Donuts & Bagels" },
      { val: "bakerySnacks", txt: "Bakery & Snacks" },
    ];

    return categories;
  }

  categoriesKFC() {
    const categories = [
      { val: "familyBucket", txt: "Family Bucket Meals" },
      { val: "famousBuckets", txt: "World Famous Buckets" },
      { val: "famousChicken", txt: "World Famous Chicken" },
      { val: "bigBoxMeals", txt: "Big Box Meals" },
      { val: "tendersNuggets", txt: "Tenders & Nuggets" },
      { val: "kentuckyWings", txt: "Kentucky Fried Wings" },
      { val: "fillUps", txt: "Fill Ups" },
      { val: "bowls", txt: "Bowls" },
      { val: "sandwiches", txt: "Signature Sandwiches" },
      { val: "sides", txt: "Sides" },
      { val: "aLaCarte", txt: "A La Carte" },
      { val: "dessert", txt: "Dessert" },
      { val: "beverages", txt: "Beverages" },
    ];

    return categories;
  }

  categoriesMcDonalds() {
    const breakfastCategories = [
      { val: "bfComboMeals", txt: "Combo Meals" },
      { val: "homestyleBreakfasts", txt: "Homestyle Breakfasts" },
      { val: "mccafe", txt: "McCafé" },
      { val: "mccafeBakery", txt: "McCafé Bakery" },
      { val: "bfSnacksSides", txt: "Snacks, Sides & More" },
      { val: "bfBeverages", txt: "Beverages" },
      { val: "bfIndividual", txt: "Individual Items" },
    ];

    const dayCategories = [
      { val: "comboMeals", txt: "Combo Meals" },
      { val: "shareables", txt: "Shareables" },
      { val: "happyMeal", txt: "Happy Meal" },
      { val: "mccafe", txt: "McCafé" },
      { val: "mccafeBakery", txt: "McCafé Bakery" },
      { val: "friesSides", txt: "Fries, Sides & More" },
      { val: "sweetsTreats", txt: "Sweets & Treats" },
      { val: "beverages", txt: "Beverages" },
      { val: "individual", txt: "Individual Items" },
    ];

    return this.currDate.getHours() >= 6 && this.currDate.getHours() < 11
      ? breakfastCategories
      : dayCategories;
  }

  categoriesStarbucks() {
    const categories = [
      { val: "hotCoffees", txt: "Hot Coffees" },
      { val: "hotTeas", txt: "Hot Teas" },
      { val: "hotDrinks", txt: "Hot Drinks" },
      { val: "frappuccino", txt: "Frappuccino Blended Beverages" },
      { val: "coldCoffees", txt: "Cold Coffees" },
      { val: "icedTeas", txt: "Iced Teas" },
      { val: "coldDrinks", txt: "Cold Drinks" },
      { val: "hotBreakfast", txt: "Hot Breakfast" },
      { val: "bakery", txt: "Bakery" },
      { val: "lunch", txt: "Lunch" },
      { val: "snacksSweets", txt: "Snacks & Sweets" },
      { val: "oatmealYogurt", txt: "Oatmeal & Yogurt" },
    ];

    return categories;
  }

  categoriesSubway() {
    const categories = [
      { val: "sandwiches", txt: "All Sandwiches" },
      { val: "freshMelts", txt: "Fresh Melts" },
      { val: "proteinBowls", txt: "Protein Bowls" },
      { val: "wraps", txt: "Wraps" },
      { val: "breakfast", txt: "Breakfast" },
      { val: "salads", txt: "Salads" },
      { val: "sides", txt: "Sides" },
      { val: "drinks", txt: "Drinks" },
    ];

    return categories;
  }

  categoriesTacoBell() {
    const categories = [
      { val: "featured", txt: "Featured" },
      { val: "combos", txt: "Combos" },
      { val: "groups", txt: "Groups" },
      { val: "specialties", txt: "Specialties" },
      { val: "tacos", txt: "Tacos" },
      { val: "burritos", txt: "Burritos" },
      { val: "quesadillas", txt: "Quesadillas" },
      { val: "nachos", txt: "Nachos" },
      { val: "valueMenu", txt: "Value Menu" },
      { val: "sides", txt: "Sides" },
      { val: "drinks", txt: "Drinks" },
      { val: "bowls", txt: "Bowls" },
      { val: "veggie", txt: "Veggie Cravings" },
    ];

    return categories;
  }

  categoriesWendys() {
    const categories = [
      { val: "breakfastCombos", txt: "Breakfast Combos" },
      { val: "croissants", txt: "Croissants" },
      { val: "biscuits", txt: "Biscuits" },
      { val: "classics", txt: "Classics" },
      { val: "coffee", txt: "Coffee" },
      { val: "sides", txt: "Sides & More" },
      { val: "beverages", txt: "Beverages" },
    ];

    return categories;
  }
}
