export default [
  {
    name: "Family Bundle",
    price: 14.99,
    img: "https://d1ralsognjng37.cloudfront.net/8730ceb8-37a3-4eff-99a6-10663b2d43a5.jpeg",
  },
  {
    name: "Ultimate Party Bundle",
    price: 30.0,
    img: "https://d1ralsognjng37.cloudfront.net/30c0fe92-9ed4-41c8-bb14-db7e2bf13ecc.jpeg",
  },
  {
    name: "Build Your Own Meal Craver",
    price: 10.0,
    img: "https://d1ralsognjng37.cloudfront.net/044eb47c-f134-48d9-a520-8fc7303f3c65.jpeg",
  },
  {
    name: "Build Your Own Meal Saver",
    price: 12,
    img: "https://d1ralsognjng37.cloudfront.net/518b9363-a54b-463c-9250-e99f5c56da6f.jpeg",
  },
  {
    name: "Build Your Own Meal Super Saver",
    price: 15,
    img: "https://d1ralsognjng37.cloudfront.net/c2ad6c7e-3920-451b-8be5-b242b6e894fe.jpeg",
  },
  {
    name: "Family Bundle Classic",
    price: 30,
    img: "https://d1ralsognjng37.cloudfront.net/9d483096-d66e-4497-b5cc-7e09b99c2afc.jpeg",
  },
  {
    name: "Family Bundle Crown",
    price: 30,
    img: "https://d1ralsognjng37.cloudfront.net/85f7a58a-acfb-4f69-a1ec-b1e894d4e711.jpeg",
  },
];
