export default [
  {
    name: "King Jr Meal - 4 Pc Nuggets",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/17e87704-ca12-467f-940b-ee16de823957.jpeg",
  },
  {
    name: "King Jr Meal - Hamburger",
    price: 5.16,
    img: "https://d1ralsognjng37.cloudfront.net/020552a1-0622-4a74-a56f-988df53d0757.jpeg",
  },
  {
    name: "King Jr Meal - Cheeseburger",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/d4f6e654-8543-4852-b343-a9a98147f758.jpeg",
  },
  {
    name: "King Jr Meal - Double Cheeseburger",
    price: 5.85,
    img: "https://d1ralsognjng37.cloudfront.net/726b3f84-b701-46b0-9e4b-d89a49fd2fb9.jpeg",
  },
];
