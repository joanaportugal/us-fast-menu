export default [
  {
    name: "Double Sausage Sourdough Breakfast King",
    price: 5.28,
    img: "https://d1ralsognjng37.cloudfront.net/fbc19f5a-fede-4c4c-b14e-d675d2fd3e7f.jpeg",
  },
  {
    name: "Double Bacon Sourdough Breakfast King",
    price: 5.38,
    img: "https://d1ralsognjng37.cloudfront.net/7cb1d4db-19ca-4758-b28b-f7e13730029b.jpeg",
  },
  {
    name: "Double Ham Sourdough Breakfast King",
    price: 5.38,
    img: "https://d1ralsognjng37.cloudfront.net/b56c2b81-9171-49f7-aab8-665e25faa675.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Croissan'wich",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/4de707f7-1be0-40ac-997d-96d2d403bca2.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Croissan'wich",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/952eff49-faff-4d8e-9725-2e9f7439ea63.jpeg",
  },
  {
    name: "Ham, Egg & Cheese Croissan'wich",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/73e5c793-4296-44d7-9216-791b44d0ddaa.jpeg",
  },
  {
    name: "Sausage & Egg Croissan'wich",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Bacon & Egg Croissan'wich",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Ham & Egg Croissan'wich",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Egg Croissan'wich",
    price: 3.09,
    img: "https://d1ralsognjng37.cloudfront.net/aff3bb9b-a777-4f1f-b845-db34c6f07eb1.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Biscuit",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/3e164d78-8d46-4eb8-9b6a-ff2468d586f0.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Biscuit",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/07a4d60f-a429-4581-b741-2e407331d092.jpeg",
  },
  {
    name: "Ham, Egg & Cheese Biscuit",
    price: 4.82,
    img: "https://d1ralsognjng37.cloudfront.net/d5f1e12e-c3da-455f-ad56-c5b19fac7cac.jpeg",
  },
  {
    name: "Sausage & Egg Biscuit",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Bacon & Egg Biscuit",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Ham & Egg Biscuit",
    price: 3.44,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Egg & Cheese Biscuit",
    price: 3.09,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Egg-Normous Burrito",
    price: 5.05,
    img: "https://d1ralsognjng37.cloudfront.net/e061b113-f01a-49b2-8acc-af3380e8e442.jpeg",
  },
  {
    name: "Pancakes & Sausage Platter",
    price: 4.36,
    img: "https://d1ralsognjng37.cloudfront.net/6996b5ab-641a-4628-a677-0ef3165fec83.jpeg",
  },
  {
    name: "French Toast - 5 Pc",
    price: 3.44,
    img: "https://d1ralsognjng37.cloudfront.net/d23703e5-cf51-43cb-86b2-8bb5f435f15f.jpeg",
  },
];
