export default [
  {
    name: "8pc Jalapeno Cheddar Bites",
    price: 2.52,
    img: "https://d1ralsognjng37.cloudfront.net/66cac567-2297-4259-9d45-389fd73f15f5.jpeg",
  },
  {
    name: "4pc Jalapeno Cheddar Bites",
    price: 1.37,
    img: "https://d1ralsognjng37.cloudfront.net/9e414dc7-7273-4639-86af-18752856b458.jpeg",
  },
  {
    name: "French Fries",
    price: 1.15,
    img: "https://d1ralsognjng37.cloudfront.net/98f3f8f0-2bbc-40f9-b7b3-cf4a3312c9f3.jpeg",
  },
  {
    name: "Onion Rings",
    price: 2.17,
    img: "https://d1ralsognjng37.cloudfront.net/128c525a-ef3e-481a-b89f-09e4d78a9a2b.jpeg",
  },
  {
    name: "Garden Side Salad",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/b40b8fb8-a25c-48ad-b6bf-5d44e6b16c23.jpeg",
  },
];
