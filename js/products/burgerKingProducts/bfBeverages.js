export default [
  {
    name: "Soft Drink",
    price: 2.98,
    img: "https://d1ralsognjng37.cloudfront.net/5f13211d-35a1-4431-8020-b10eac87d0d9.jpeg",
  },
  {
    name: "Iced Tea",
    price: 2.98,
    img: "https://d1ralsognjng37.cloudfront.net/4b6923c8-ee04-4546-a5bf-bfd99c09c649.jpeg",
  },
  {
    name: "Simply Orange Juice",
    price: 1.15,
    img: "https://d1ralsognjng37.cloudfront.net/3195a0bd-05cf-47b8-a95f-a7c127bccce7.jpeg",
  },
  {
    name: "Capri Sun Apple Juice",
    price: 1.55,
    img: "https://d1ralsognjng37.cloudfront.net/be818847-e5c6-4353-9fdc-03580a5c8268.jpeg",
  },
  {
    name: "Bottled Nestlé Pure Life Purified Water",
    price: 1.15,
    img: "https://d1ralsognjng37.cloudfront.net/1409d131-b585-483e-92b6-9b250bd0f129.jpeg",
  },
  {
    name: "Fat Free Milk",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/1166208e-cd58-44cb-afbd-2ec94612d170.jpeg",
  },
  {
    name: "Chocolate Milk",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/c038bb45-d583-4ee5-847b-c0756b0331ae.jpeg",
  },
  {
    name: "BK Café Coffee",
    price: 1.83,
    img: "https://d1ralsognjng37.cloudfront.net/f3b9772d-fcf0-4c90-aa10-02a120c819e1.jpeg",
  },
  {
    name: "BK Café Vanilla Iced Coffee - Medium",
    price: 2.29,
    img: "https://d1ralsognjng37.cloudfront.net/562999dc-53a5-4049-8906-7bcc94306144.jpeg",
  },
  {
    name: "BK Café Mocha Iced Coffee - Medium",
    price: 2.06,
    img: "https://d1ralsognjng37.cloudfront.net/98985006-1e6e-4baf-912b-2a2e92993414.jpeg",
  },
];
