export default [
  {
    name: "Ch'King Sandwich Meal",
    price: 10.9,
    img: "https://d1ralsognjng37.cloudfront.net/6b1a5bd5-537d-4be2-9733-1803394322cb.jpeg",
  },
  {
    name: "Ch'King Deluxe Sandwich Meal",
    price: 11.71,
    img: "https://d1ralsognjng37.cloudfront.net/12157547-7308-4547-9bdc-7b2ef75f9622.jpeg",
  },
  {
    name: "Spicy Ch'King Sandwich Meal",
    price: 10.9,
    img: "https://d1ralsognjng37.cloudfront.net/deb2c58f-a811-4b16-9888-5f2789150002.jpeg",
  },
  {
    name: "Spicy Ch'King Deluxe Sandwich Meal",
    price: 11.71,
    img: "https://d1ralsognjng37.cloudfront.net/3b3aab92-c7e9-413a-b802-8afad9e95b49.jpeg",
  },
  {
    name: "Whopper Meal",
    price: 11.48,
    img: "https://d1ralsognjng37.cloudfront.net/a907c985-52f5-4891-bcd3-002f36642333.jpeg",
  },
  {
    name: "Double Whopper Meal",
    price: 12.63,
    img: "https://d1ralsognjng37.cloudfront.net/dac8920b-a7bb-4933-b86c-43e98d5c6adb.jpeg",
  },
  {
    name: "Triple Whopper Meal",
    price: 13.78,
    img: "https://d1ralsognjng37.cloudfront.net/58565bf9-7d00-4f87-9999-940149ee7df3.jpeg",
  },
  {
    name: "Impossible Whopper Meal",
    price: 12.63,
    img: "https://d1ralsognjng37.cloudfront.net/02026a0f-9efe-4c33-b24e-15d88b4a7f12.jpeg",
  },
  {
    name: "Whopper Jr. Meal",
    price: 7.91,
    img: "https://d1ralsognjng37.cloudfront.net/4ce822b8-41d4-4bb2-a529-5e07f01e9211.jpeg",
  },
  {
    name: "Chicken Fries - 9 Pc Meal",
    price: 9.64,
    img: "https://d1ralsognjng37.cloudfront.net/6c9edbbf-a2de-45f1-a635-269ee8b72ef8.jpeg",
  },
  {
    name: "Big Fish Sandwich Meal",
    price: 10.79,
    img: "https://d1ralsognjng37.cloudfront.net/ae66cb3e-83b3-49e1-8916-94b8066472b6.jpeg",
  },
  {
    name: "Single Bacon King Meal",
    price: 10.67,
    img: "https://d1ralsognjng37.cloudfront.net/fa8ced59-3f9c-4eb6-ae3a-61ea4bcc685f.jpeg",
  },
];
