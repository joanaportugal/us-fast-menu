export default [
  {
    name: "Soft Drink",
    price: 2.98,
    img: "https://d1ralsognjng37.cloudfront.net/5f13211d-35a1-4431-8020-b10eac87d0d9.jpeg",
  },
  {
    name: "Frozen Coke",
    price: 1.94,
    img: "https://d1ralsognjng37.cloudfront.net/42d457dd-0e31-41a7-9cf9-6373a683bf8d.jpeg",
  },
  {
    name: "Frozen Fanta Wild Cherry",
    price: 1.94,
    img: "https://d1ralsognjng37.cloudfront.net/0180bec0-626d-4af6-8a16-d8ee86f026e8.jpeg",
  },
  {
    name: "Frozen Strawberry Lemonade",
    price: 2.06,
    img: "https://d1ralsognjng37.cloudfront.net/35ffbbf0-0974-405e-9d65-78caed259ca1.jpeg",
  },
  {
    name: "Capri Sun Apple Juice",
    price: 1.55,
    img: "https://d1ralsognjng37.cloudfront.net/be818847-e5c6-4353-9fdc-03580a5c8268.jpeg",
  },
  {
    name: "Simply Orange Juice",
    price: 1.15,
    img: "https://d1ralsognjng37.cloudfront.net/3195a0bd-05cf-47b8-a95f-a7c127bccce7.jpeg",
  },
  {
    name: "Fat Free Milk",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/1166208e-cd58-44cb-afbd-2ec94612d170.jpeg",
  },
  {
    name: "Chocolate Milk",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/c038bb45-d583-4ee5-847b-c0756b0331ae.jpeg",
  },
  {
    name: "Bottled Nestlé Pure Life Purified Water",
    price: 1.15,
    img: "https://d1ralsognjng37.cloudfront.net/1409d131-b585-483e-92b6-9b250bd0f129.jpeg",
  },
  {
    name: "Iced Tea",
    price: 2.98,
    img: "https://d1ralsognjng37.cloudfront.net/4b6923c8-ee04-4546-a5bf-bfd99c09c649.jpeg",
  },
];
