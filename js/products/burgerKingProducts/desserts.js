export default [
  {
    name: "HERSHEY'S Sundae Pie",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/e72cf2d3-b2c7-4d78-8457-851b0fe21d89.jpeg",
  },
  {
    name: "Soft Serve Cup",
    price: 1.94,
    img: "https://d1ralsognjng37.cloudfront.net/0062cf53-6257-4528-b48c-bff888fd07aa.jpeg",
  },
  {
    name: "Sundae",
    price: 1.71,
    img: "https://d1ralsognjng37.cloudfront.net/85022883-f6de-4ec8-aec6-40dd9dcd5038.jpeg",
  },
  {
    name: "Classic OREO Shake",
    price: 4.47,
    img: "https://d1ralsognjng37.cloudfront.net/737379a5-c1c3-4cc1-bdd5-5aa530871d71.jpeg",
  },
  {
    name: "Chocolate Shake",
    price: 4.47,
    img: "https://d1ralsognjng37.cloudfront.net/952b783f-0ea7-4684-9224-142c0f35be59.jpeg",
  },
  {
    name: "Vanilla Shake",
    price: 4.47,
    img: "https://d1ralsognjng37.cloudfront.net/76f5224a-14e9-427c-b86c-a599e6486c8b.jpeg",
  },
  {
    name: "Chocolate OREO Shake",
    price: 4.47,
    img: "https://d1ralsognjng37.cloudfront.net/0e344973-a181-4e73-a064-110e178d9273.jpeg",
  },
];
