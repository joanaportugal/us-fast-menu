export default [
  {
    name: "Hash Browns",
    price: 2.17,
    img: "https://d1ralsognjng37.cloudfront.net/17996c98-02f8-426a-8cd4-d93420617cb0.jpeg",
  },
  {
    name: "Pancakes",
    price: 3.78,
    img: "https://d1ralsognjng37.cloudfront.net/5c81ac42-742f-444f-9186-36769a94ca8a.jpeg",
  },
  {
    name: "French Toast Sticks - 5 Pc",
    price: 3.44,
    img: "https://d1ralsognjng37.cloudfront.net/d23703e5-cf51-43cb-86b2-8bb5f435f15f.jpeg",
  },
];
