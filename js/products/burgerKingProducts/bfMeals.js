export default [
  {
    name: "Double Sausage Sourdough Breakfast King Meal",
    price: 8.26,
    img: "https://d1ralsognjng37.cloudfront.net/8f3cedae-fcdd-4bb1-bc7c-6e8f048248c5.jpeg",
  },
  {
    name: "Double Bacon Sourdough Breakfast King Meal",
    price: 8.26,
    img: "https://d1ralsognjng37.cloudfront.net/e4c7e673-f3ed-467c-9d45-9a32a5a38068.jpeg",
  },
  {
    name: "Double Ham Sourdough Breakfast King Meal",
    price: 8.26,
    img: "https://d1ralsognjng37.cloudfront.net/94714b18-a9e3-4fbb-8f11-b935923a87c5.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Croissan'wich Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/604bf256-2b81-48c9-ba32-07c3ed491658.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Croissan'wich Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/6db710fb-c906-49c4-a0f6-5faca494e516.jpeg",
  },
  {
    name: "Ham, Egg & Cheese Croissan'wich Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/1cba3108-0264-4be3-ae9b-f5048026ce0e.jpeg",
  },
  {
    name: "Sausage & Egg Croissan'wich Meal",
    price: 6.07,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Bacon & Egg Croissan'wich Meal",
    price: 6.07,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Ham & Egg Croissan'wich Meal",
    price: 6.07,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Egg & Cheese Croissan'wich Meal",
    price: 5.5,
    img: "https://d1ralsognjng37.cloudfront.net/a1bcc5f1-9e7e-472e-a0a8-c03f0ceb27c4.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Biscuit Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/49041012-5ff7-4e2b-86e4-eb42a362c892.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Biscuit Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/7ebaa655-25c5-4d56-af7b-532769f4639b.jpeg",
  },
  {
    name: "Ham, Egg & Cheese Biscuit Meal",
    price: 8.6,
    img: "https://d1ralsognjng37.cloudfront.net/bb6d1aa1-7c83-441c-97b8-bc5f01aaf4df.jpeg",
  },
  {
    name: "Egg & Cheese Biscuit Meal",
    price: 5.73,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Sausage & Cheese Biscuit Meal",
    price: 5.73,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Bacon & Cheese Biscuit Meal",
    price: 5.73,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Ham & Cheese Biscuit Meal",
    price: 5.73,
    img: "https://www.marshopping.com/matosinhos/-/media/images/b2c/portugal/matosinhos/images-stores/burger-king/bk-logo-410x282.ashx?h=282&iar=0&mw=650&w=410&hash=162DD52D29CAB7886BE6AC9848AACA4E",
  },
  {
    name: "Egg-Normous Burrito Meal",
    price: 9.06,
    img: "https://d1ralsognjng37.cloudfront.net/9522a5f7-2065-46ab-8bc6-f183a550d6f8.jpeg",
  },
  {
    name: "2 Breakfast Burrito Jr. Meal",
    price: 5.73,
    img: "https://d1ralsognjng37.cloudfront.net/5d469b78-c1f1-4630-a974-9d31c8e59926.jpeg",
  },
  {
    name: "French Toast - 5 Pc Meal",
    price: 7.22,
    img: "https://d1ralsognjng37.cloudfront.net/6a040d84-1eba-41b7-9008-f3221c15a55b.jpeg",
  },
];
