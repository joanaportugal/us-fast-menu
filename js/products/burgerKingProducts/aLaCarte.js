export default [
  {
    name: "Ch'King Sandwich",
    price: 5.74,
    img: "https://d1ralsognjng37.cloudfront.net/5dae5557-f6dd-44c9-8f56-1505a31bb93f.jpeg",
  },
  {
    name: "Ch'King Deluxe Sandwich",
    price: 6.54,
    img: "https://d1ralsognjng37.cloudfront.net/93d9bccc-530e-4679-8c2b-10e5280bfde4.jpeg",
  },
  {
    name: "Spicy Ch'King Sandwich",
    price: 5.74,
    img: "https://d1ralsognjng37.cloudfront.net/90b09eb0-a11f-4251-9a2c-593aa47dbbe0.jpeg",
  },
  {
    name: "Spicy Ch'King Deluxe Sandwich",
    price: 6.54,
    img: "https://d1ralsognjng37.cloudfront.net/8f78c663-f25a-41f4-98b5-b8d9429f3919.jpeg",
  },
  {
    name: "Whopper",
    price: 6.89,
    img: "https://d1ralsognjng37.cloudfront.net/13d991a8-3c22-4030-aeee-3c02ea01f89f.jpeg",
  },
  {
    name: "Double Whopper",
    price: 8.04,
    img: "https://d1ralsognjng37.cloudfront.net/d0c8632f-dca5-4b7e-bc8a-28d1fe9a66f1.jpeg",
  },
  {
    name: "Tripple Whopper",
    price: 9.19,
    img: "https://d1ralsognjng37.cloudfront.net/c3b1bb69-da3c-4d6e-b94b-ba796d388f3c.jpeg",
  },
  {
    name: "Impossible Whopper",
    price: 8.04,
    img: "https://d1ralsognjng37.cloudfront.net/0d4b48cf-c39c-4f8c-a462-d57c61134bcc.jpeg",
  },
  {
    name: "Chicken Nuggets - 10 Pc",
    price: 1.71,
    img: "https://d1ralsognjng37.cloudfront.net/20e94b8b-b00d-4281-ba06-47d78322f1d2.jpeg",
  },
  {
    name: "Chicken Fries - 9 Pc",
    price: 5.05,
    img: "https://d1ralsognjng37.cloudfront.net/553c7992-3b83-410d-a507-a41cb26101a0.jpeg",
  },
  {
    name: "Single Bacon King",
    price: 6.66,
    img: "https://d1ralsognjng37.cloudfront.net/3ecdd2b0-97d1-4860-9a96-3a598cc3caa4.jpeg",
  },
  {
    name: "Big Fish Sandwich",
    price: 6.2,
    img: "https://d1ralsognjng37.cloudfront.net/c27a8aae-1d47-4a24-a903-4aee1a16257f.jpeg",
  },
  {
    name: "Crispy Chicken Club Salad",
    price: 6.89,
    img: "https://d1ralsognjng37.cloudfront.net/1f719294-b1ac-4c9f-a756-754b6c7192bf.jpeg",
  },
  {
    name: "Crispy Chicken Garden Salad",
    price: 5.89,
    img: "https://d1ralsognjng37.cloudfront.net/351bbb07-b5bf-4709-af1a-5e5d7d13a3f0.jpeg",
  },
];
