import bfMeals from "./burgerKingProducts/bfMeals.js";
import bfEntrees from "./burgerKingProducts/bfEntrees.js";
import bfBeverages from "./burgerKingProducts/bfBeverages.js";
import bfSides from "./burgerKingProducts/bfSides.js";
import familyBundles from "./burgerKingProducts/familyBundles.js";
import meals from "./burgerKingProducts/meals.js";
import aLaCarte from "./burgerKingProducts/aLaCarte.js";
import beverages from "./burgerKingProducts/beverages.js";
import sides from "./burgerKingProducts/sides.js";
import desserts from "./burgerKingProducts/desserts.js";
import kingJrMeals from "./burgerKingProducts/kingJrMeals.js";

export default {
  bfMeals,
  bfEntrees,
  bfBeverages,
  bfSides,
  familyBundles,
  meals,
  aLaCarte,
  beverages,
  sides,
  desserts,
  kingJrMeals,
};
