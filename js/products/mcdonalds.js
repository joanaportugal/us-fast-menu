import bfComboMeals from "./mcdonaldsProducts/bfComboMeals.js";
import homestyleBreakfasts from "./mcdonaldsProducts/homestyleBreakfasts.js";
import mccafe from "./mcdonaldsProducts/mccafe.js";
import mccafeBakery from "./mcdonaldsProducts/mccafeBakery.js";
import bfSnacksSides from "./mcdonaldsProducts/bfSnacksSides.js";
import bfBeverages from "./mcdonaldsProducts/bfBeverages.js";
import bfIndividual from "./mcdonaldsProducts/bfIndividual.js";

import comboMeals from "./mcdonaldsProducts/comboMeals.js";
import shareables from "./mcdonaldsProducts/shareables.js";
import happyMeal from "./mcdonaldsProducts/happyMeal.js";
import friesSides from "./mcdonaldsProducts/friesSides.js";
import sweetsTreats from "./mcdonaldsProducts/sweetsTreats.js";
import beverages from "./mcdonaldsProducts/beverages.js";
import individual from "./mcdonaldsProducts/individual.js";

export default {
  bfComboMeals,
  homestyleBreakfasts,
  mccafe,
  mccafeBakery,
  bfSnacksSides,
  bfBeverages,
  bfIndividual,
  comboMeals,
  shareables,
  happyMeal,
  friesSides,
  sweetsTreats,
  beverages,
  individual,
};
