import breakfastCombos from "./wendysProducts/breakfastCombos.js";
import croissants from "./wendysProducts/croissants.js";
import biscuits from "./wendysProducts/biscuits.js";
import classics from "./wendysProducts/classics.js";
import coffee from "./wendysProducts/coffee.js";
import sides from "./wendysProducts/sides.js";
import beverages from "./wendysProducts/beverages.js";

export default {
  breakfastCombos,
  croissants,
  biscuits,
  classics,
  coffee,
  sides,
  beverages,
};
