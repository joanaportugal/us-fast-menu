export default [
  {
    name: "Quesarito",
    price: 4.67,
    img: "https://d1ralsognjng37.cloudfront.net/143a8104-ec1c-4a2f-bf23-093c3753ec8e.jpeg",
  },
  {
    name: "Bean Burrito",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/c026fc82-97f2-4828-b711-2f42b853f257.jpeg",
  },
  {
    name: "Beefy 5-Layer Burrito",
    price: 3.95,
    img: "https://d1ralsognjng37.cloudfront.net/ffd6d071-dd78-4b0e-9a89-33f443497bfe.jpeg",
  },
  {
    name: "Burrito Supreme",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/0a2b0517-78a3-4bef-a880-8bb3333e25a5.jpeg",
  },
];
