export default [
  {
    name: "Soft Taco",
    price: 2.15,
    img: "https://d1ralsognjng37.cloudfront.net/2576fa45-2aa6-493e-8013-9196d810613b.jpeg",
  },
  {
    name: "Soft Taco Supreme",
    price: 2.99,
    img: "https://d1ralsognjng37.cloudfront.net/1a7d121b-44e8-4378-b209-96a353f67b0f.jpeg",
  },
  {
    name: "Crunchy Taco",
    price: 2.15,
    img: "https://d1ralsognjng37.cloudfront.net/f0d4fe8c-5d06-4a6b-bb04-38746e974383.jpeg",
  },
  {
    name: "Crunchy Taco Supreme",
    price: 2.99,
    img: "https://d1ralsognjng37.cloudfront.net/c16bfe3a-dcbe-4863-a1a5-7ca4a848f3a3.jpeg",
  },
  {
    name: "Cheesy Gordita Crunch",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/0d726765-e5e2-461b-8a98-8f56403d6325.jpeg",
  },
  {
    name: "Chalupa Supreme",
    price: 4.43,
    img: "https://d1ralsognjng37.cloudfront.net/b9603e81-9b75-4e8c-873a-95fdd23e3ccc.jpeg",
  },
  {
    name: "Nacho Cheese Doritos Locos Tacos",
    price: 2.87,
    img: "https://d1ralsognjng37.cloudfront.net/00c9952d-49a9-44be-b913-f79deabe96b6.jpeg",
  },
  {
    name: "Nacho Cheese Doritos Locos Tacos Supreme",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/fdb8738a-748b-471c-8fc1-63cd7daf0305.jpeg",
  },
];
