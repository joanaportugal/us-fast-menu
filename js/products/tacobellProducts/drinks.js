export default [
  {
    name: "Diet Pepsi",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/c69df4bf-8249-4370-8bc3-3f6cb395ae03.jpeg",
  },
  {
    name: "Pepsi Zero Sugar",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/19569c98-ae09-4814-8c4f-a23bdf021cec.jpeg",
  },
  {
    name: "Mountain Dew",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/3b4b9ed4-cee2-4524-9b64-392016638a3c.jpeg",
  },
  {
    name: "Mountain Dew Baja Blast",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/490ae21a-c201-4cca-ac0b-3ec21b8ac3fa.jpeg",
  },
  {
    name: "Mountain Dew Baja Blast Zero Sugar",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/68d1f7ff-5fc8-4262-b0be-d26a0be8ddd0.jpeg",
  },
  {
    name: "Mountain Dew Baja Blast Freeze",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/f9a0184f-6293-4a9f-8158-609b40c7f02a.jpeg",
  },
  {
    name: "Mountain Dew Kickstart Orange Citrus",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/7a149ef3-6c8f-4b63-94b3-9922b065192b.jpeg",
  },
  {
    name: "Sierra Mist",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/06ddb452-fd21-419c-bb7e-b7b14857e3a3.jpeg",
  },
  {
    name: "G2 Gatorade Fruit Punch",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/b4cae755-f286-4ea2-84b3-d689cfd3da14.jpeg",
  },
  {
    name: "Tropicana Pink Lemonade",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/a5b5467f-3326-46d2-98de-10baa7a4285a.jpeg",
  },
  {
    name: "Brisk Mango Fiesta",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/a7d5ed24-7985-42bc-b73d-b2a5043f504c.jpeg",
  },
  {
    name: "Lipton Unsweetened Iced Tea",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/00089f81-be50-457f-99d8-6e65196e78ed.jpeg",
  },
  {
    name: "Regular Iced Coffee",
    price: 1.91,
    img: "https://d1ralsognjng37.cloudfront.net/17c27bcb-b08a-4623-954c-8db4c27d30fc.jpeg",
  },
  {
    name: "Premium Hot Coffee",
    price: 1.91,
    img: "https://d1ralsognjng37.cloudfront.net/73d8cf3f-83d0-4d57-a13a-afd158da9524.jpeg",
  },
  {
    name: "Drinks Party Pack",
    price: 4.8,
    img: "https://d1ralsognjng37.cloudfront.net/fab3b8b9-f206-41ab-83de-afbd41a50a40.jpeg",
  },
];
