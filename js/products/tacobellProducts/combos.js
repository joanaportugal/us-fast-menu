export default [
  {
    name: "3 Doritos Locos Tacos Combo",
    price: 9.59,
    img: "https://d1ralsognjng37.cloudfront.net/8fd12bf9-e4b3-4e53-8d75-37825a83c1bc.jpeg",
  },
  {
    name: "3 Doritos Locos Tacos Supreme Combo",
    price: 10.55,
    img: "https://d1ralsognjng37.cloudfront.net/4189b063-191d-469c-b7f6-908c9f260f0d.jpeg",
  },
  {
    name: "3 Crunchy Tacos Supreme Combo",
    price: 9.83,
    img: "https://d1ralsognjng37.cloudfront.net/4189b063-191d-469c-b7f6-908c9f260f0d.jpeg",
  },
  {
    name: "3 Crunchy Tacos Combo",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/0ee44da6-5b5c-4e68-bec3-9db4c5b4f3a3.jpeg",
  },
  {
    name: "3 Soft Tacos Supreme Combo",
    price: 10.07,
    img: "https://d1ralsognjng37.cloudfront.net/c7d879be-4da7-47c3-b76a-64298c298a4a.jpeg",
  },
  {
    name: "3 Soft Tacos Combo",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/0a8bec50-433c-40af-a375-145574c64a22.jpeg",
  },
  {
    name: "Burrito Supreme Combo",
    price: 9.59,
    img: "https://d1ralsognjng37.cloudfront.net/baaa7747-91ff-4f27-8254-3134bf92939f.jpeg",
  },
  {
    name: "Crunchwrap Supreme Combo",
    price: 9.23,
    img: "https://d1ralsognjng37.cloudfront.net/e5efdbe4-d874-407f-b30a-dd6ad2c85f6d.jpeg",
  },
  {
    name: "Nachos BellGrande Combo",
    price: 9.83,
    img: "https://d1ralsognjng37.cloudfront.net/af15880a-9e9b-4fa4-86b8-a7ff1fe69b8e.jpeg",
  },
  {
    name: "2 Chicken Chalupas Supreme Combo",
    price: 11.75,
    img: "https://d1ralsognjng37.cloudfront.net/47c8a5a1-e163-4a03-8b68-21da556976d6.jpeg",
  },
  {
    name: "Chicken Quesadilla Combo",
    price: 9.47,
    img: "https://d1ralsognjng37.cloudfront.net/9a714e11-737d-41e0-82f6-1582e63f346c.jpeg",
  },
];
