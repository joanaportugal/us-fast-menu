export default [
  {
    name: "Nachos BellGrande",
    price: 5.99,
    img: "https://d1ralsognjng37.cloudfront.net/bcf4ec9f-f160-43bf-a9af-110b611c91f6.jpeg",
  },
  {
    name: "Nachos BellGrande Combo",
    price: 9.83,
    img: "https://d1ralsognjng37.cloudfront.net/af15880a-9e9b-4fa4-86b8-a7ff1fe69b8e.jpeg",
  },
  {
    name: "Chips and Nacho Cheese Sauce",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/bd423bc2-afa1-4b20-aa05-67359acefe86.jpeg",
  },
];
