export default [
  {
    name: "Black Bean Chalupa",
    price: 4.43,
    img: "https://d1ralsognjng37.cloudfront.net/698e7e78-932d-438b-8a2a-0de447eb6884.jpeg",
  },
  {
    name: "Cheese Quesadilla",
    price: 4.67,
    img: "https://d1ralsognjng37.cloudfront.net/779c096b-5364-426d-9f74-98bd9c265c1b.jpeg",
  },
  {
    name: "Bean Burrito",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/c026fc82-97f2-4828-b711-2f42b853f257.jpeg",
  },
  {
    name: "Cheesy Roll Up",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/4c1e3644-4788-464e-8940-13c39aae7488.jpeg",
  },
  {
    name: "Cheesy Bean and Rice Burrito",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/3eba8e66-f4dc-4739-aa48-38b1f34fcb92.jpeg",
  },
  {
    name: "Black Bean Quesarito",
    price: 4.67,
    img: "https://d1ralsognjng37.cloudfront.net/8c703fe9-65f2-4a34-8c59-aaf222b066fd.jpeg",
  },
  {
    name: "Black Bean Crunchwrap Supreme",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/08a7c653-5299-483c-9d2c-af67acbb623d.jpeg",
  },
];
