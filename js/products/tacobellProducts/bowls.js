export default [
  {
    name: "Power Menu Bowl",
    price: 7.79,
    img: "https://d1ralsognjng37.cloudfront.net/51ed0e15-352c-445b-b4e3-b8b8be0b9b7c.jpeg",
  },
  {
    name: "Power Menu Bowl - Veggie",
    price: 7.67,
    img: "https://d1ralsognjng37.cloudfront.net/29680d52-bc9c-4fa1-91a9-3659dd5ee8c1.jpeg",
  },
];
