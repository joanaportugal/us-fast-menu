export default [
  {
    name: "Chicken Chipotle Melt",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/e3fa2fbc-a513-4b05-8dee-d456a2cb7687.jpeg",
  },
  {
    name: "Beef Burrito",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/ce5b1587-0dad-48f5-aeb4-2559e32adaa7.jpeg",
  },
  {
    name: "Cheesy Bean and Rice Burrito",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/3eba8e66-f4dc-4739-aa48-38b1f34fcb92.jpeg",
  },
  {
    name: "Cheesy Roll Up",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/4c1e3644-4788-464e-8940-13c39aae7488.jpeg",
  },
  {
    name: "Chips and Nacho Cheese Sauce",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/bd423bc2-afa1-4b20-aa05-67359acefe86.jpeg",
  },
  {
    name: "Cinnamon Twists",
    price: 1.67,
    img: "https://d1ralsognjng37.cloudfront.net/0b2511c0-01bd-4f0f-bf00-95f2c89245fc.jpeg",
  },
];
