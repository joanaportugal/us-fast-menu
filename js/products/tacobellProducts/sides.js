export default [
  {
    name: "Black Beans",
    price: 1.91,
    img: "https://d1ralsognjng37.cloudfront.net/ba379405-1311-4ba1-8c17-64aa72d2bfaf.jpeg",
  },
  {
    name: "Black Beans and Rice",
    price: 1.91,
    img: "https://d1ralsognjng37.cloudfront.net/d5ef8918-e8b1-4555-80e9-5b187e43cccd.jpeg",
  },
];
