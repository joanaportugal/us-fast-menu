export default [
  {
    name: "Taco & Burrito Cravings Pack",
    price: 17.99,
    img: "https://d1ralsognjng37.cloudfront.net/52dc9b34-9320-4e25-a93d-06fc010a5b16.jpeg",
  },
  {
    name: "Taco Party Pack",
    price: 22.79,
    img: "https://d1ralsognjng37.cloudfront.net/55267231-0e11-4f7c-9b40-c6e61840fa1d.jpeg",
  },
  {
    name: "Soft Taco Party Pack",
    price: 22.79,
    img: "https://d1ralsognjng37.cloudfront.net/4ec6f090-026a-45c8-9ccd-530447931284.jpeg",
  },
  {
    name: "Supreme Taco Party Pack",
    price: 26.39,
    img: "https://d1ralsognjng37.cloudfront.net/2c16a499-7531-440d-8563-fa564bce9017.jpeg",
  },
  {
    name: "Supreme Soft Taco Party Pack",
    price: 27.59,
    img: "https://d1ralsognjng37.cloudfront.net/97c261d8-95d2-4a63-a2bb-89f41bcd7a08.jpeg",
  },
];
