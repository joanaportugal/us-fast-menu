export default [
  {
    name: "Chicken Quesadilla",
    price: 5.99,
    img: "https://d1ralsognjng37.cloudfront.net/799b5670-bb04-4f91-9119-3260706b1a93.jpeg",
  },
  {
    name: "Chicken Quesadilla Combo",
    price: 9.47,
    img: "https://d1ralsognjng37.cloudfront.net/9a714e11-737d-41e0-82f6-1582e63f346c.jpeg",
  },
  {
    name: "Steak Quesadilla",
    price: 5.99,
    img: "https://d1ralsognjng37.cloudfront.net/5781c101-15b6-4295-9ea8-2f8588c5d020.jpeg",
  },
  {
    name: "Cheese Quesadilla",
    price: 4.67,
    img: "https://d1ralsognjng37.cloudfront.net/779c096b-5364-426d-9f74-98bd9c265c1b.jpeg",
  },
];
