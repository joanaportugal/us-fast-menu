export default [
  {
    name: "Black Bean Chalupa",
    price: 4.43,
    img: "https://d1ralsognjng37.cloudfront.net/698e7e78-932d-438b-8a2a-0de447eb6884.jpeg",
  },
  {
    name: "Crunchwrap Supreme",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/ef91c8d7-dbd7-4660-ab4c-32179c0b18b7.jpeg",
  },
  {
    name: "Chalupa Supreme",
    price: 4.43,
    img: "https://d1ralsognjng37.cloudfront.net/b9603e81-9b75-4e8c-873a-95fdd23e3ccc.jpeg",
  },
  {
    name: "Cheesy Gordita Crunch",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/0d726765-e5e2-461b-8a98-8f56403d6325.jpeg",
  },
];
