export default [
  {
    name: "Chicken Pack",
    price: 27.99,
    img: "https://d1ralsognjng37.cloudfront.net/dc4f64b0-7332-4c9b-a2e6-98f40759867c.jpeg",
  },
  {
    name: "40 McNuggets",
    price: 15.69,
    img: "https://d1ralsognjng37.cloudfront.net/c1392f2a-fadb-4d10-beac-2606962d237c.jpeg",
  },
  {
    name: "Classic Big Mac Pack",
    price: 25.59,
    img: "https://d1ralsognjng37.cloudfront.net/ae434347-9595-4432-8764-4d0d8740293b.jpeg",
  },
  {
    name: "13 Cookie Tote",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/11da5a72-54b0-4e74-87e9-c29a82d63c34.jpeg",
  },
];
