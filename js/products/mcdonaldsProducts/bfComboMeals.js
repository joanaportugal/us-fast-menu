export default [
  {
    name: "Bacon Egg Cheese Biscuit Meal",
    price: 8.09,
    img: "https://d1ralsognjng37.cloudfront.net/634293ff-8ac6-485b-8c54-210640f191c7.jpeg",
  },
  {
    name: "Sausage Egg Biscuit Meal",
    price: 7.89,
    img: "https://d1ralsognjng37.cloudfront.net/affe19f9-d8f5-4440-8a3c-93fd1f3b93fd.jpeg",
  },
  {
    name: "McChicken Biscuit Meal",
    price: 5.49,
    img: "https://d1ralsognjng37.cloudfront.net/53402156-267c-4f44-9cc1-28c80ec0c4c3.jpeg",
  },
  {
    name: "Egg McMuffin Meal",
    price: 7.69,
    img: "https://d1ralsognjng37.cloudfront.net/99c3ef57-637e-48f3-b1db-00e3e27bc31e.jpeg",
  },
  {
    name: "Sausage Egg McMuffin Meal",
    price: 8.19,
    img: "https://d1ralsognjng37.cloudfront.net/b8c4e5d4-9f9d-4e89-8356-5f4d8b239c56.jpeg",
  },
  {
    name: "Sausage McMuffin Meal",
    price: 5.09,
    img: "https://d1ralsognjng37.cloudfront.net/41224343-3cb8-42bb-afad-2ed7543903c3.jpeg",
  },
  {
    name: "Bacon Egg Cheese McGriddle Meal",
    price: 8.29,
    img: "https://d1ralsognjng37.cloudfront.net/d480ad87-5fbe-4314-ae3a-ca56ec7ee4d1.jpeg",
  },
  {
    name: "Sausage Egg Cheese McGriddle Meal",
    price: 8.79,
    img: "https://d1ralsognjng37.cloudfront.net/f2c7bb5c-680f-4852-806f-5d68ad8e6381.jpeg",
  },
  {
    name: "Chicken McGriddle Meal",
    price: 5.49,
    img: "https://d1ralsognjng37.cloudfront.net/9c6734fd-6851-4873-b430-baad4c3e81c0.jpeg",
  },
  {
    name: "2 Sausage Burrito Meal",
    price: 7.69,
    img: "https://d1ralsognjng37.cloudfront.net/1faf9838-4028-458b-8eae-31e441bb58db.jpeg",
  },
  {
    name: "Bacon Egg Cheese Bagel Meal",
    price: 8.19,
    img: "https://d1ralsognjng37.cloudfront.net/7c05c679-c219-430a-9a1a-f16b078de1d2.jpeg",
  },
  {
    name: "Steak Egg Cheese Bagel Meal",
    price: 9.49,
    img: "https://d1ralsognjng37.cloudfront.net/b4c94c01-7c4c-409c-95a1-b9cf0678ef45.jpeg",
  },
];
