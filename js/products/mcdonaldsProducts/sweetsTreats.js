export default [
  {
    name: "Medium Chocolate Shake",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/11c19892-6eb7-4845-ae91-c8b58d79fe16.jpeg",
  },
  {
    name: "Medium Strawberry Shake",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/b66cfe7b-9f7a-4e8d-a0d5-8e2e7eb9c38b.jpeg",
  },
  {
    name: "Medium Vanilla Shake",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/79739420-2c1e-4c5c-83f6-06ab98c2eb82.jpeg",
  },
  {
    name: "Regular M&M McFlurry",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/33a5e90a-96e3-48f5-a618-8f30823ca5d0.jpeg",
  },
  {
    name: "Regular Oreo McFlurry",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/543e2ebb-385f-4cb3-8615-c14658648a2a.jpeg",
  },
  {
    name: "Medium Minute Maid Strawberry Watermelon Slushie",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/40c395ed-5136-4da7-92fb-90dd5821bda7.jpeg",
  },
  {
    name: "Medium Blue Raspberry Slushie Minute Maid",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/ac3663a3-3f44-4d78-a9ec-219000cdc43b.jpeg",
  },
  {
    name: "Medium Pink Lemonade Slushie Minute Maid",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/ba47d1c3-124e-41ac-988b-7ac259614eea.jpeg",
  },
  {
    name: "Caramel Sundae",
    price: 2.59,
    img: "https://d1ralsognjng37.cloudfront.net/6c95db6b-7cb5-438a-9a2e-c213acac17a1.jpeg",
  },
  {
    name: "Hot Fudge Sundae",
    price: 2.59,
    img: "https://d1ralsognjng37.cloudfront.net/45b20335-186b-49ac-853d-123791f722b2.jpeg",
  },
  {
    name: "1 Cookie",
    price: 0.99,
    img: "https://d1ralsognjng37.cloudfront.net/4dd3e1ca-2ed6-4587-a67a-3ba895f9f345.jpeg",
  },
  {
    name: "2 Cookies",
    price: 1.49,
    img: "https://d1ralsognjng37.cloudfront.net/58de2c4d-8e99-4600-b457-cc3aea5d6fa7.jpeg",
  },
  {
    name: "13 Cookie Tote",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/11da5a72-54b0-4e74-87e9-c29a82d63c34.jpeg",
  },
  {
    name: "Apple Pie",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/895a84d2-9b67-45eb-95e7-eb7481bc4df5.jpeg",
  },
  {
    name: "Strawberry & Crème Pie",
    price: 1.79,
    img: "https://d1ralsognjng37.cloudfront.net/d7f50547-0a6a-4686-9100-005a0664be2a.jpeg",
  },
];
