export default [
  {
    name: "Medium Hi-C Orange",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/87cca113-0542-4c74-a822-edb0e0e856e7.jpeg",
  },
  {
    name: "Medium Coke",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/31b6a471-e165-41b4-9c11-13062bd3af85.jpeg",
  },
  {
    name: "Medium Diet Coke",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/081f4b05-42cd-4ae3-a963-109ed4fa8431.jpeg",
  },
  {
    name: "Medium Sprite",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/1f5f73b5-dac6-4aea-9534-4766f0be4e32.jpeg",
  },
  {
    name: "Medium Fanta Orange",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/dedb5f93-9dc1-4088-8405-02aed3e5889a.jpeg",
  },
  {
    name: "Medium Dr Pepper",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/70fe2707-073c-46b6-b4b0-cfc6347594df.jpeg",
  },
  {
    name: "Medium Hawaiian Punch",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/0137938c-0cca-4d9d-84a1-1ca2eec92024.jpeg",
  },
  {
    name: "Medium Root Beer",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/453d2af9-7f14-494a-87a2-2d39766a540d.jpeg",
  },
  {
    name: "Medium Minute Maid Orange Juice",
    price: 3.09,
    img: "https://d1ralsognjng37.cloudfront.net/ec0760ea-a36c-4710-b654-35503b4e52c2.jpeg",
  },
  {
    name: "Dasani Bottled Water",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/763b3b56-76d3-41ce-ae20-2f57a06fd12f.jpeg",
  },
  {
    name: "Milk",
    price: 1.29,
    img: "https://d1ralsognjng37.cloudfront.net/527e1263-a37c-446c-8b2d-29d181814edb.jpeg",
  },
  {
    name: "Honest Kids Organic Apple Juice Drink",
    price: 1.29,
    img: "https://d1ralsognjng37.cloudfront.net/05c127d9-1ecf-496b-a251-90f056460202.jpeg",
  },
  {
    name: "Medium Sweet Iced Tea",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/a7478710-0dfc-4f70-bc81-1785273e235e.jpeg",
  },
  {
    name: "Medium Hot Tea",
    price: 1.69,
    img: "https://d1ralsognjng37.cloudfront.net/43ca2f0b-1bcf-43e0-8691-64c7c5d86353.jpeg",
  },
  {
    name: "Medium Unsweetened Iced Tea",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/66735c8f-2fd4-4716-a7d5-029fd073b17f.jpeg",
  },
  {
    name: "Medium Frozen Sprite Lymonade",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/3fb23eb0-33b6-4b64-a068-d876d3c94205.jpeg",
  },
  {
    name: "Medium Frozen Coca Cola Classic",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/c19a3beb-19e8-4b30-8cca-cf42c5e93fdb.jpeg",
  },
  {
    name: "Medium Frozen Fanta Wild Cherry",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/45f20820-b929-4e82-9e2b-90c2dc0bff63.jpeg",
  },
];
