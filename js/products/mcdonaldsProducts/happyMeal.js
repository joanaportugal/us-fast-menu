export default [
  {
    name: "Hamburger - Happy Meal",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/8b626693-7f6b-434d-a3b1-634a4892fd99.jpeg",
  },
  {
    name: "4 Piece Chicken McNugget - Happy Meal",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/0ea7603c-38f5-4880-8c0d-4bd09cfd1adb.jpeg",
  },
  {
    name: "6 Piece Chicken McNugget - Happy Meal",
    price: 5.59,
    img: "https://d1ralsognjng37.cloudfront.net/2f09ff84-1a86-4c02-9cbe-684043969919.jpeg",
  },
];
