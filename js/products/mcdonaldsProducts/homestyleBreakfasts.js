export default [
  {
    name: "Big Breakfast",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/166fdf66-ba49-4168-b5ba-482b3803f695.jpeg",
  },
  {
    name: "Big Breakfast with Hotcakes",
    price: 7.09,
    img: "https://d1ralsognjng37.cloudfront.net/8297667e-bbea-4cb2-a406-f92e564434de.jpeg",
  },
  {
    name: "Big Breakfast with Steak and Hotcakes",
    price: 7.09,
    img: "https://d1ralsognjng37.cloudfront.net/2f503107-2976-40f2-8584-43567fc252e8.jpeg",
  },
  {
    name: "Hotcakes and Sausage",
    price: 4.99,
    img: "https://d1ralsognjng37.cloudfront.net/c0062dff-bff7-4d0d-b514-050a4c214f35.jpeg",
  },
  {
    name: "Hotcakes",
    price: 3.79,
    img: "https://d1ralsognjng37.cloudfront.net/33931d74-0a91-4126-97f4-11d02b559e73.jpeg",
  },
];
