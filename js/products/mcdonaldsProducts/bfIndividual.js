export default [
  {
    name: "Bacon Egg Cheese Biscuit",
    price: 4.99,
    img: "https://d1ralsognjng37.cloudfront.net/60c059d9-12fe-449b-829a-01dc2c57a2dd.jpeg",
  },
  {
    name: "Sausage Egg Biscuit",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/4201a6a5-64b2-498d-a277-93f4daf41036.jpeg",
  },
  {
    name: "Sausage Biscuit",
    price: 1.79,
    img: "https://d1ralsognjng37.cloudfront.net/47561b55-1a0f-4ccd-adce-6f5298ae30ce.jpeg",
  },
  {
    name: "McChicken Biscuit",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/2f40dd62-0f0e-4c90-a0f4-ddc829ff5ee4.jpeg",
  },
  {
    name: "Hot N Spicy McChicken Biscuit",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/ec2edff4-df9b-4e55-a374-db665787a9e3.jpeg",
  },
  {
    name: "Sausage Egg Cheese Biscuit",
    price: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/1bfb9a81-7203-4db3-a7fc-b843e2d03acb.jpeg",
  },
  {
    name: "Biscuit",
    price: 1.69,
    img: "https://d1ralsognjng37.cloudfront.net/4c579dc1-4f8e-48e9-8ff8-1ae9faa96660.jpeg",
  },
  {
    name: "Egg McMuffin",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/44ae39f3-51b7-4a85-ab74-85251b23c0bc.jpeg",
  },
  {
    name: "Sausage Egg McMuffin",
    price: 4.59,
    img: "https://d1ralsognjng37.cloudfront.net/c3b45fd9-ff51-4a0c-bbcf-4f8f8e9cf517.jpeg",
  },
  {
    name: "Sausage McMuffin",
    price: 1.69,
    img: "https://d1ralsognjng37.cloudfront.net/7d37beef-bb1f-40c1-9e7b-81692edb9827.jpeg",
  },
  {
    name: "Bacon Egg Cheese McGriddle",
    price: 5.19,
    img: "https://d1ralsognjng37.cloudfront.net/d0b539bb-abfe-4b34-be69-c2f05880f69e.jpeg",
  },
  {
    name: "Sausage Egg Cheese McGriddle",
    price: 5.59,
    img: "https://d1ralsognjng37.cloudfront.net/5dd0bcb6-885a-4c88-83f3-de6bec05b7a2.jpeg",
  },
  {
    name: "Sausage McGriddle",
    price: 2.79,
    img: "https://d1ralsognjng37.cloudfront.net/003cdde0-231b-4e75-85ad-169e6f6f8afd.jpeg",
  },
  {
    name: "Chicken McGriddle",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/fdcddfb9-9db0-4de3-b8a4-215e7b92e6b4.jpeg",
  },
  {
    name: "Hot & Spicy Chicken McGriddle",
    price: 2.29,
    img: "https://d1ralsognjng37.cloudfront.net/d91490b7-821c-4000-b9f8-4e207aded7be.jpeg",
  },
  {
    name: "Sausage Burrito",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/94b8d1cd-5205-4365-b1ab-05627aa5a865.jpeg",
  },
  {
    name: "Bacon Egg Cheese Bagel",
    price: 5.09,
    img: "https://d1ralsognjng37.cloudfront.net/46d3240c-b675-4975-8844-88d1f8d00b9d.jpeg",
  },
  {
    name: "Steak Egg Cheese Bagel",
    price: 6.79,
    img: "https://d1ralsognjng37.cloudfront.net/5df1b387-f41e-4271-8d41-6780fde04b26.jpeg",
  },
];
