export default [
  {
    name: "McCafé Blueberry Muffin",
    price: 2.59,
    img: "https://d1ralsognjng37.cloudfront.net/c3e7c31a-b2ea-4b3a-89da-6db96304fa40.jpeg",
  },
  {
    name: "McCafé Apple Fritter",
    price: 2.79,
    img: "https://d1ralsognjng37.cloudfront.net/ffef7470-f1b4-4aa1-a51a-d95248acb58f.jpeg",
  },
  {
    name: "McCafé Cinnamon Roll",
    price: 3.19,
    img: "https://d1ralsognjng37.cloudfront.net/2be25249-af18-4e75-adb4-0dd0adb8e2a1.jpeg",
  },
  {
    name: "1 Cookie",
    price: 0.99,
    img: "https://d1ralsognjng37.cloudfront.net/4dd3e1ca-2ed6-4587-a67a-3ba895f9f345.jpeg",
  },
  {
    name: "2 Cookies",
    price: 1.49,
    img: "https://d1ralsognjng37.cloudfront.net/58de2c4d-8e99-4600-b457-cc3aea5d6fa7.jpeg",
  },
  {
    name: "13 Cookie Tote",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/11da5a72-54b0-4e74-87e9-c29a82d63c34.jpeg",
  },
  {
    name: "Apple Pie",
    price: 1.59,
    img: "https://d1ralsognjng37.cloudfront.net/895a84d2-9b67-45eb-95e7-eb7481bc4df5.jpeg",
  },
  {
    name: "Strawberry & Crème Pie",
    price: 1.79,
    img: "https://d1ralsognjng37.cloudfront.net/d7f50547-0a6a-4686-9100-005a0664be2a.jpeg",
  },
];
