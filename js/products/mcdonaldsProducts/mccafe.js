export default [
  {
    name: "Medium Premium Roast Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/cd7152df-8df2-4ab8-80b4-ec3a6a4b618b.jpeg",
  },
  {
    name: "Medium Decaf Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/2597e32c-cdf3-47a5-aa56-9eddf59fd2d7.jpeg",
  },
  {
    name: "Medium Hot Tea",
    price: 1.69,
    img: "https://d1ralsognjng37.cloudfront.net/43ca2f0b-1bcf-43e0-8691-64c7c5d86353.jpeg",
  },
  {
    name: "Medium Iced Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/42bab3b8-1727-42c1-832d-d4f61a5d185d.jpeg",
  },
  {
    name: "Medium Iced Caramel Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/4aef33ae-e21e-44a8-9c86-7e909418a791.jpeg",
  },
  {
    name: "Medium Iced French Vanilla Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/a0e2ef30-3321-4ddf-8e33-c0c728e994f1.jpeg",
  },
  {
    name: "Medium Iced Sugar Free Vanilla Coffee",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/a098946c-0116-40ed-ade6-3ff5d6b1a31d.jpeg",
  },
  {
    name: "Medium Premium Hot Chocolate",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/eb3c4e9b-5807-4e0d-8992-e8c3cb7ccf92.jpeg",
  },
  {
    name: "Medium Caramel Hot Chocolate",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/b158540c-28a7-446a-9d20-d9df3f16357f.jpeg",
  },
  {
    name: "Medium Minute Maid Strawberry Watermelon Slushie (Only breakfast)",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/40c395ed-5136-4da7-92fb-90dd5821bda7.jpeg",
  },
  {
    name: "Medium Blue Raspberry Slushie Minute Maid (Only breakfast)",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/ac3663a3-3f44-4d78-a9ec-219000cdc43b.jpeg",
  },
  {
    name: "Medium Pink Lemonade Slushie Minute Maid (Only breakfast)",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/ba47d1c3-124e-41ac-988b-7ac259614eea.jpeg",
  },
  {
    name: "Medium Mocha Frappé",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/fd04783e-1d50-42ae-91cc-0f8c7f8f48a8.jpeg",
  },
  {
    name: "Medium Caramel Macchiato",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/9118a4ef-0207-4655-8a9a-bfd179a0c240.jpeg",
  },
  {
    name: "Medium Iced Caramel Macchiato",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/1bd32b15-d83c-4aff-a912-ddc75fd458f4.jpeg",
  },
  {
    name: "Medium Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/e47d570d-9e62-4987-ae23-2d682ce1b69e.jpeg",
  },
  {
    name: "Medium Nonfat Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/584bcbd9-82f6-41c1-ba32-7f86f644cb81.jpeg",
  },
  {
    name: "Medium Caramel Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/9a118f7d-8ccc-4117-9384-8f19d41defef.jpeg",
  },
  {
    name: "Medium Nonfat Caramel Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/1b3d7c2c-636a-4ea8-9ecf-02e86fdfe079.jpeg",
  },
  {
    name: "Medium Iced Mocha",
    price: 3.69,
    img: "https://d1ralsognjng37.cloudfront.net/65725bf9-342e-4182-9ed1-a86287aea441.jpeg",
  },
  {
    name: "Medium Iced Nonfat Mocha",
    price: 3.69,
    img: "https://d1ralsognjng37.cloudfront.net/bcfff35f-ae78-4c94-99d3-d2ae1d687c96.jpeg",
  },
  {
    name: "Medium Iced Caramel Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/4eff09e9-3933-48f0-bbb9-f52359d52482.jpeg",
  },
  {
    name: "Medium Iced Nonfat Caramel Mocha",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/3b53c9a7-624b-4ea5-8da4-b177372fd309.jpeg",
  },
  {
    name: "Medium Strawberry Banana Smoothie",
    price: 3.49,
    img: "https://d1ralsognjng37.cloudfront.net/1a0ac619-487d-4476-a851-3e4952a0d8c9.jpeg",
  },
  {
    name: "Medium Mango Pineapple Smoothie",
    price: 3.49,
    img: "https://d1ralsognjng37.cloudfront.net/901d1376-afbe-43f6-b03a-4643f6a8d4f8.jpeg",
  },
  {
    name: "Medium Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/5ed52ccb-b7d4-486c-9254-89d76b3fba8e.jpeg",
  },
  {
    name: "Medium Nonfat Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/66d101b7-a368-451a-835e-63dc24bfbe7c.jpeg",
  },
  {
    name: "Medium Caramel Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/a75018d7-c514-43fd-80e0-af7c6da055f7.jpeg",
  },
  {
    name: "Medium French Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/69e27b33-fb7c-4d9c-854d-f653b4be7319.jpeg",
  },
  {
    name: "Medium Nonfat French Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/7785e433-fdb1-4f4b-8289-d2a40346efee.jpeg",
  },
  {
    name: "Medium Sugar Free Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/51695793-0f90-4d43-b526-a60fcca929cc.jpeg",
  },
  {
    name: "Medium Iced Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/ab525305-4559-4aac-b5a0-d4f4588efc1e.jpeg",
  },
  {
    name: "Medium Iced Caramel Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/a7a318be-78fb-462e-b60b-02c220735acd.jpeg",
  },
  {
    name: "Medium Iced French Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/edaabfd8-0627-4bee-bdb3-39f0573f0975.jpeg",
  },
  {
    name: "Medium Iced Nonfat French Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/9f3590fb-cd7a-4366-82e5-b71311e5d69f.jpeg",
  },
  {
    name: "Medium Iced Sugar Free Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/edaabfd8-0627-4bee-bdb3-39f0573f0975.jpeg",
  },
  {
    name: "Medium Iced Nonfat Sugar Free Vanilla Latte",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/5d91d75c-0670-4223-a928-3d9d83246e68.jpeg",
  },
  {
    name: "Medium Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/3a09bc7a-d6b0-4c78-af90-c9e5c3746ce4.jpeg",
  },
  {
    name: "Medium Nonfat Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/63588db8-95e3-4e59-a224-f05591d59044.jpeg",
  },
  {
    name: "Medium Vanilla Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/9585e592-c665-4b49-8874-8322c9b5dd10.jpeg",
  },
  {
    name: "Medium Nonfat Vanilla Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/ab01322c-8fc9-44df-9df9-a6784d80e090.jpeg",
  },
  {
    name: "Medium  Sugarfree Vanilla Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/9585e592-c665-4b49-8874-8322c9b5dd10.jpeg",
  },
  {
    name: "Medium Caramel Cappucino",
    price: 3.29,
    img: "https://d1ralsognjng37.cloudfront.net/4f999023-774b-4981-a391-d5f2b5d7166d.jpeg",
  },
  {
    name: "Medium Americano",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/a692f3ae-7190-4002-8d31-8b011b98f1d8.jpeg",
  },
];
