export default [
  {
    name: "Medium French Fries",
    price: 3.19,
    img: "https://d1ralsognjng37.cloudfront.net/c4e708fe-05ce-42d5-8984-e4697ccc1ad3.jpeg",
  },
  {
    name: "Basket of Fries",
    price: 4.19,
    img: "https://d1ralsognjng37.cloudfront.net/a7046cba-51d9-4c21-be1b-13a8ffd5a9db.jpeg",
  },
  {
    name: "Apple Slices",
    price: 0.99,
    img: "https://d1ralsognjng37.cloudfront.net/b49edaf8-2ba7-4a67-a1b6-57967b9f3c0d.jpeg",
  },
];
