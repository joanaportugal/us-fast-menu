export default [
  {
    name: "Hash Browns",
    price: 2.29,
    img: "https://d1ralsognjng37.cloudfront.net/94449602-944f-4444-ad9c-7780f38ca4aa.jpeg",
  },
  {
    name: "Oatmeal",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/36789637-592a-4d56-83b8-774f0ac01be0.jpeg",
  },
  {
    name: "Apple Slices",
    price: 0.99,
    img: "https://d1ralsognjng37.cloudfront.net/b49edaf8-2ba7-4a67-a1b6-57967b9f3c0d.jpeg",
  },
  {
    name: "Sausage",
    price: 2.19,
    img: "https://d1ralsognjng37.cloudfront.net/1e47fa2f-e7dc-4a76-9bd0-0fa6b5449c8e.jpeg",
  },
  {
    name: "Scrambled Eggs",
    price: 2.99,
    img: "https://d1ralsognjng37.cloudfront.net/b9167e6b-698f-4485-810c-da777a952f01.jpeg",
  },
];
