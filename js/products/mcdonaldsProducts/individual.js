export default [
  {
    name: "Crispy Chicken Sandwich",
    price: 5.49,
    img: "https://d1ralsognjng37.cloudfront.net/a0a33295-823e-447a-a83a-73d3b241776d.jpeg",
  },
  {
    name: "Spicy Crispy Chicken Sandwich",
    price: 5.69,
    img: "https://d1ralsognjng37.cloudfront.net/b17833f0-873e-415f-b394-b186f697f082.jpeg",
  },
  {
    name: "Deluxe Crispy Chicken Sandwich",
    price: 6.19,
    img: "https://d1ralsognjng37.cloudfront.net/35690206-39c2-4d6b-807a-3df82087ae48.jpeg",
  },
  {
    name: "Spicy Crispy Chicken Sandwich",
    price: 6.39,
    img: "https://d1ralsognjng37.cloudfront.net/af28df88-28e5-4c85-8e53-4f034a45c0cd.jpeg",
  },
  {
    name: "Big Mac",
    price: 6.29,
    img: "https://d1ralsognjng37.cloudfront.net/27092939-3f19-421b-8e77-831d7884a656.jpeg",
  },
  {
    name: "Cheeseburger",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/096bdc3a-2971-4618-8d95-59db683052bb.jpeg",
  },
  {
    name: "2 Cheeseburger",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/1b0ec9cd-bdb2-46a1-abd9-5cd7e92ab648.jpeg",
  },
  {
    name: "Triple Cheeseburger",
    price: 4.09,
    img: "https://d1ralsognjng37.cloudfront.net/1b0ec9cd-bdb2-46a1-abd9-5cd7e92ab648.jpeg",
  },
  {
    name: "Double Bacon Quarter Pounder with Cheese",
    price: 8.49,
    img: "https://d1ralsognjng37.cloudfront.net/ee6b3a1d-5aa0-4100-96b4-7532df1eeda8.jpeg",
  },
  {
    name: "Quarter Pounder with Cheese",
    price: 6.39,
    img: "https://d1ralsognjng37.cloudfront.net/90343ee5-0e25-4832-bdb9-e021b084d674.jpeg",
  },
  {
    name: "Bacon Quarter Pounder with Cheese",
    price: 6.89,
    img: "https://d1ralsognjng37.cloudfront.net/bdf48172-e388-4645-b0a0-9cbcd78db8b5.jpeg",
  },
  {
    name: "Quarter Pounder with Cheese Deluxe",
    price: 6.79,
    img: "https://d1ralsognjng37.cloudfront.net/77824255-448a-4eca-ac50-39a5d58a634e.jpeg",
  },
  {
    name: "Hamburger",
    price: 2.29,
    img: "https://d1ralsognjng37.cloudfront.net/757b11f5-c9d5-44a4-b202-828f273128c1.jpeg",
  },
  {
    name: "Bacon McDouble",
    price: 4.49,
    img: "https://d1ralsognjng37.cloudfront.net/f99ff1d7-04b5-4e35-a00b-fd7c47573840.jpeg",
  },
  {
    name: "McDouble",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/07bfddbf-0813-48e1-a00b-68a723e7f4d7.jpeg",
  },
  {
    name: "4 Piece McNuggets",
    price: 2.59,
    img: "https://d1ralsognjng37.cloudfront.net/efd72940-2e33-414a-ad08-d2eb959e4658.jpeg",
  },
  {
    name: "6 Piece McNuggets",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/b636ab63-ef52-4fcb-85f7-9d13d3e414cc.jpeg",
  },
  {
    name: "10 Piece McNuggets",
    price: 5.79,
    img: "https://d1ralsognjng37.cloudfront.net/f61ebb12-f68d-4bd2-895b-1c70870d1288.jpeg",
  },
  {
    name: "20 Piece McNuggets",
    price: 8.29,
    img: "https://d1ralsognjng37.cloudfront.net/a0493974-9fca-4b24-8e75-853fb7f0b4bc.jpeg",
  },
  {
    name: "40 Piece McNuggets",
    price: 15.69,
    img: "https://d1ralsognjng37.cloudfront.net/c1392f2a-fadb-4d10-beac-2606962d237c.jpeg",
  },
  {
    name: "Filet O Fish",
    price: 5.19,
    img: "https://d1ralsognjng37.cloudfront.net/c5633a50-6fe3-4090-9110-c5e38ccd57da.jpeg",
  },
  {
    name: "McChicken",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/769c8345-6207-43eb-9f6e-1fd3ba9f5f2c.jpeg",
  },
  {
    name: "Hot and Spicy McChicken",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/b3a8fb5b-5586-4248-ab25-36d12a7f645b.jpeg",
  },
];
