export default [
  {
    name: "Crispy Chicken Sandwich Meal",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/ed023e5d-550b-4914-8a1c-d87890a793fc.jpeg",
  },
  {
    name: "Spicy Chicken Sandwich Meal",
    price: 9.39,
    img: "https://d1ralsognjng37.cloudfront.net/089c978d-72b9-4924-a86c-69b236831e0b.jpeg",
  },
  {
    name: "Deluxe Crispy Chicken Sandwich Meal",
    price: 9.79,
    img: "https://d1ralsognjng37.cloudfront.net/96be60bf-ab2c-42ba-89aa-468a39a599d7.jpeg",
  },
  {
    name: "Spicy Deluxe Crispy Chicken Sandwich Meal",
    price: 10.09,
    img: "https://d1ralsognjng37.cloudfront.net/2aad03a5-5c68-4cba-a8f0-2b3b916d4d67.jpeg",
  },
  {
    name: "Big Mac Meal",
    price: 10.19,
    img: "https://d1ralsognjng37.cloudfront.net/a0be323d-0108-4a80-b3e8-79c29a0cdf56.jpeg",
  },
  {
    name: "2 Cheeseburger Meal",
    price: 8.49,
    img: "https://d1ralsognjng37.cloudfront.net/260e1bd0-2fae-4ebf-8c31-e830d9256ac7.jpeg",
  },
  {
    name: "Double Bacon Quarter Pounder with Cheese Meal",
    price: 11.79,
    img: "https://d1ralsognjng37.cloudfront.net/a59fd3eb-7e2d-493d-90eb-48c274eb462c.jpeg",
  },
  {
    name: "Double Quarter Pounder with Cheese Meal",
    price: 11.59,
    img: "https://d1ralsognjng37.cloudfront.net/5460c2fd-7893-4f19-b0be-659cd20300cb.jpeg",
  },
  {
    name: "Quarter Pounder with Cheese Meal",
    price: 10.19,
    img: "https://d1ralsognjng37.cloudfront.net/4b7a1963-0e7a-41e3-beac-2a8c4467b5c5.jpeg",
  },
  {
    name: "Bacon Quarter Pounder with Cheese Meal",
    price: 10.39,
    img: "https://d1ralsognjng37.cloudfront.net/cdaba73c-99c3-49ba-9e75-b4a0a12776b1.jpeg",
  },
  {
    name: "Quarter Pounder with Cheese Deluxe Meal",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/140fffd3-4c30-4bdd-aa22-a35a6262c181.jpeg",
  },
  {
    name: "10 Piece McNuggets Meal",
    price: 9.79,
    img: "https://d1ralsognjng37.cloudfront.net/dc3967f2-b709-4fd3-974a-5b20484bfb31.jpeg",
  },
  {
    name: "Filet O Fish Meal",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/a8b1e45d-40b9-46f2-aa6d-35e9ff6f36eb.jpeg",
  },
  {
    name: "Hot and Spicy McChicken Meal",
    price: 6.09,
    img: "https://d1ralsognjng37.cloudfront.net/9ae938ff-9c77-4f59-b0e8-beaf98b75f85.jpeg",
  },
];
