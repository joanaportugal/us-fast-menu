import hotCoffees from "./starbucksProducts/hotCoffees.js";
import hotTeas from "./starbucksProducts/hotTeas.js";
import hotDrinks from "./starbucksProducts/hotDrinks.js";
import frappuccino from "./starbucksProducts/frappuccino.js";
import coldCoffees from "./starbucksProducts/coldCoffees.js";
import icedTeas from "./starbucksProducts/icedTeas.js";
import coldDrinks from "./starbucksProducts/coldDrinks.js";
import hotBreakfast from "./starbucksProducts/hotBreakfast.js";
import bakery from "./starbucksProducts/bakery.js";
import lunch from "./starbucksProducts/lunch.js";
import snacksSweets from "./starbucksProducts/snacksSweets.js";
import oatmealYogurt from "./starbucksProducts/oatmealYogurt.js";

export default {
  hotCoffees,
  hotTeas,
  hotDrinks,
  frappuccino,
  coldCoffees,
  icedTeas,
  coldDrinks,
  hotBreakfast,
  bakery,
  lunch,
  snacksSweets,
  oatmealYogurt,
};
