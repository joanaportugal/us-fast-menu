export default [
  {
    name: "Seasoned Potatoes",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/9d1abd3f-0ee6-4d1a-8d2e-aa70ecd4cd36.jpeg",
  },
  {
    name: "Oatmeal Bar",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/10290e6d-cefe-4171-b913-6da78bb27ad4.jpeg",
  },
  {
    name: "Apple Bites",
    price: 1.16,
    img: "https://d1ralsognjng37.cloudfront.net/e58ae1f2-63d8-4a1f-a9e9-af429b27e1aa.jpeg",
  },
  {
    name: "Strawberries",
    price: 2.1,
    img: "https://d1ralsognjng37.cloudfront.net/15cb8f36-ee38-4877-833d-f0d80ee67b1f.jpeg",
  },
  {
    name: "Honey Butter Biscuit",
    price: 1.4,
    img: "https://d1ralsognjng37.cloudfront.net/0ae45581-738c-47c2-aa66-0088dd9bd4cb.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Burrito",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/99c141ff-e85e-4726-8f8c-7d1215315469.jpeg",
  },
  {
    name: "Sausage Biscuit",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/657783ee-5fc3-4407-9ff0-e76dc53be1ad.jpeg",
  },
  {
    name: "Simply Orange Juice",
    price: 3.16,
    img: "https://d1ralsognjng37.cloudfront.net/6d4f9a27-a0ac-495c-a680-48bea0470488.jpeg",
  },
];
