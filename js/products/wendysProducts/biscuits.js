export default [
  {
    name: "Sausage, Egg & Cheese Biscuit",
    price: 3.98,
    img: "https://d1ralsognjng37.cloudfront.net/d7d8aff4-0b45-4c47-8d98-554b050c7078.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Biscuit",
    price: 3.98,
    img: "https://d1ralsognjng37.cloudfront.net/e1bc3a72-c2e2-491c-a41f-b1c771b59599.jpeg",
  },
  {
    name: "Honey Butter Chicken Biscuit",
    price: 4.1,
    img: "https://d1ralsognjng37.cloudfront.net/3cebf5ea-505e-4bc1-abc8-7cb175951a43.jpeg",
  },
  {
    name: "Honey Butter Biscuit",
    price: 1.4,
    img: "https://d1ralsognjng37.cloudfront.net/0ae45581-738c-47c2-aa66-0088dd9bd4cb.jpeg",
  },
  {
    name: "Sausage Gravy & Biscuit",
    price: 2.81,
    img: "https://d1ralsognjng37.cloudfront.net/6631e91e-ddb6-427b-8a13-cb2abaec5007.jpeg",
  },
  {
    name: "Sausage Biscuit",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/657783ee-5fc3-4407-9ff0-e76dc53be1ad.jpeg",
  },
];
