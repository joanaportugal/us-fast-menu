export default [
  {
    name: "Maple Bacon Chicken Croissant Combo",
    price: 7.63,
    img: "https://d1ralsognjng37.cloudfront.net/ae7fbe11-c083-454d-93bc-d59eb92d84c4.jpeg",
  },
  {
    name: "Sausage, Egg & Swiss Croissant Combo",
    price: 7.04,
    img: "https://d1ralsognjng37.cloudfront.net/7bfb8be4-b687-41e7-a5fe-84dc7dee5a46.jpeg",
  },
  {
    name: "Bacon, Egg & Swiss Croissant Combo",
    price: 7.04,
    img: "https://d1ralsognjng37.cloudfront.net/d2102983-108c-4137-9825-db31847b6093.jpeg",
  },
  {
    name: "Honey Butter Chicken Biscuit Combo",
    price: 6.57,
    img: "https://d1ralsognjng37.cloudfront.net/80d5081c-9a6d-4713-a3ff-0927d7c29ab1.jpeg",
  },
  {
    name: "Sausage, Egg & Cheese Biscuit Combo",
    price: 6.45,
    img: "https://d1ralsognjng37.cloudfront.net/796bb937-e218-4400-84dd-ab02a483ebec.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Biscuit Combo",
    price: 6.45,
    img: "https://d1ralsognjng37.cloudfront.net/1504ace8-4583-4574-a422-094539966bf1.jpeg",
  },
  {
    name: "Breakfast Baconator Combo",
    price: 7.63,
    img: "https://d1ralsognjng37.cloudfront.net/6d58cf8d-6533-4723-9826-a9b0d2628204.jpeg",
  },
  {
    name: "Classic Sausage, Egg & Cheese Sandwich Combo",
    price: 6.8,
    img: "https://d1ralsognjng37.cloudfront.net/cd135898-9aa5-45b5-82f7-95c9882ebe9c.jpeg",
  },
  {
    name: "Classic Bacon, Egg & Cheese Sandwich Combo",
    price: 6.8,
    img: "https://d1ralsognjng37.cloudfront.net/b68a8370-261c-4cb5-8ae8-c6bba0f19cdf.jpeg",
  },
];
