export default [
  {
    name: "Cold Brew Iced Coffee",
    price: 3.16,
    img: "https://d1ralsognjng37.cloudfront.net/747bd0b3-a822-4aeb-ac61-12c1261b8211.jpeg",
  },
  {
    name: "Vanilla Frosty-ccino",
    price: 3.16,
    img: "https://d1ralsognjng37.cloudfront.net/bb49c3e4-5be9-4854-b4eb-24afc2e573cc.jpeg",
  },
  {
    name: "Chocolate Frosty-ccino",
    price: 3.16,
    img: "https://d1ralsognjng37.cloudfront.net/c4857618-5bc2-4a2f-909c-4b7f9bfebb66.jpeg",
  },
  {
    name: "Fresh Brewed Coffee",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/2f26f686-9ac3-46c5-b63e-4367a21112df.jpeg",
  },
  {
    name: "Fresh Brewed Decaffeinated Coffee",
    price: 1.99,
    img: "https://d1ralsognjng37.cloudfront.net/a385ce93-217f-4a02-9895-4c2500ff9ec4.jpeg",
  },
];
