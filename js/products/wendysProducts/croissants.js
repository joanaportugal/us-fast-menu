export default [
  {
    name: "Maple Bacon Chicken Croissant",
    price: 5.16,
    img: "https://d1ralsognjng37.cloudfront.net/64a8c9a5-1f00-40dc-80f4-befe9ba06781.jpeg",
  },
  {
    name: "Sausage, Egg & Swiss Croissant",
    price: 4.57,
    img: "https://d1ralsognjng37.cloudfront.net/c002e5c1-672d-4f0b-835d-5dd963cf1f46.jpeg",
  },
  {
    name: "Bacon, Egg & Swiss Croissant",
    price: 4.57,
    img: "https://d1ralsognjng37.cloudfront.net/1df364db-34a5-4bd8-9eb9-ff3f0d823070.jpeg",
  },
];
