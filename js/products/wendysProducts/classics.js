export default [
  {
    name: "Breakfast Baconator",
    price: 5.16,
    img: "https://d1ralsognjng37.cloudfront.net/a6966e1f-74ef-4d40-89bc-cb77ec0fcc36.jpeg",
  },
  {
    name: "Classic Sausage, Egg & Cheese Sandwich",
    price: 4.34,
    img: "https://d1ralsognjng37.cloudfront.net/bf01c422-1aff-4448-9f77-f7edcb4b933f.jpeg",
  },
  {
    name: "Classic Bacon, Egg & Cheese Sandwich",
    price: 4.34,
    img: "https://d1ralsognjng37.cloudfront.net/4cb9cff5-318b-4e46-a1e4-fc7eafe154f9.jpeg",
  },
];
