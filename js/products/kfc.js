import familyBucket from "./kfcProducts/familyBucket.js";
import famousBuckets from "./kfcProducts/famousBuckets.js";
import famousChicken from "./kfcProducts/famousChicken.js";
import bigBoxMeals from "./kfcProducts/bigBoxMeals.js";
import tendersNuggets from "./kfcProducts/tendersNuggets.js";
import kentuckyWings from "./kfcProducts/kentuckyWings.js";
import fillUps from "./kfcProducts/fillUps.js";
import bowls from "./kfcProducts/bowls.js";
import sandwiches from "./kfcProducts/sandwiches.js";
import sides from "./kfcProducts/sides.js";
import aLaCarte from "./kfcProducts/aLaCarte.js";
import dessert from "./kfcProducts/dessert.js";
import beverages from "./kfcProducts/beverages.js";

export default {
  familyBucket,
  famousBuckets,
  famousChicken,
  bigBoxMeals,
  tendersNuggets,
  kentuckyWings,
  fillUps,
  bowls,
  sandwiches,
  sides,
  aLaCarte,
  dessert,
  beverages,
};
