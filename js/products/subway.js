import sandwiches from "./subwayProducts/sandwiches.js";
import freshMelts from "./subwayProducts/freshMelts.js";
import proteinBowls from "./subwayProducts/proteinBowls.js";
import wraps from "./subwayProducts/wraps.js";
import breakfast from "./subwayProducts/breakfast.js";
import salads from "./subwayProducts/salads.js";
import sides from "./subwayProducts/sides.js";
import drinks from "./subwayProducts/drinks.js";

export default {
  sandwiches,
  freshMelts,
  proteinBowls,
  wraps,
  breakfast,
  salads,
  sides,
  drinks,
};
