export default [
  {
    name: "Frozen Matcha Latte",
    priceSmall: 4.98,
    priceMedium: 5.61,
    priceLarge: 6.23,
    img: "https://d1ralsognjng37.cloudfront.net/25b13fc9-5584-452d-8750-2d513abba744.jpeg",
  },
  {
    name: "Frozen Coffee",
    priceSmall: 4.98,
    priceMedium: 5.61,
    priceLarge: 6.23,
    img: "https://d1ralsognjng37.cloudfront.net/b29d4ecc-72a4-4650-846f-47e247315772.jpeg",
  },
  {
    name: "Frozen Chocolate",
    priceSmall: 4.98,
    priceMedium: 5.61,
    priceLarge: 6.23,
    img: "https://d1ralsognjng37.cloudfront.net/c627f2a7-f7a7-4cfc-b86f-86095850f9fd.jpeg",
  },
  {
    name: "Coolatta",
    priceSmall: 4.98,
    priceMedium: 5.61,
    priceLarge: 6.23,
    img: "https://d1ralsognjng37.cloudfront.net/ceb8364f-a805-4821-b7fb-147e18e342ee.jpeg",
  },
];
