export default [
  {
    name: "Ham & Cheese Rollups",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/38925de9-774f-4774-908e-c0713f33cdcf.png",
  },
  {
    name: "Bacon & Cheese Rollups",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/696089b4-d263-42ba-9889-d5dbd487dd7d.png",
  },
  {
    name: "Three Cheese Croissant Stuffer",
    price: 4.36,
    img: "https://d1ralsognjng37.cloudfront.net/9769dde2-a311-4337-848a-124e7cdcc289.jpeg",
  },
  {
    name: "Chicken, Bacon & Cheese Croissant Stuffer",
    price: 4.36,
    img: "https://d1ralsognjng37.cloudfront.net/0c13bd67-610a-4791-9a20-f89eac0774b9.jpeg",
  },
  {
    name: "Sweet Black Pepper Snackin' Bacon",
    price: 3.11,
    img: "https://d1ralsognjng37.cloudfront.net/167278ef-21ff-4493-8880-5033d6ed5901.jpeg",
  },
  {
    name: "Plain Stuffed Bagel Minis",
    price: 3.11,
    img: "https://d1ralsognjng37.cloudfront.net/3d11932a-abe5-42d5-9dd2-f3594a7dbdc6.jpeg",
  },
  {
    name: "Everything Stuffed Bagel Minis",
    price: 3.11,
    img: "https://d1ralsognjng37.cloudfront.net/6365cc79-c6dd-4cc4-95a7-1bc83d38307f.jpeg",
  },
  {
    name: "Muffin",
    price: 2.48,
    img: "https://d1ralsognjng37.cloudfront.net/43455e24-dc84-47b0-8fe3-af590cffd307.jpeg",
  },
  {
    name: "4 Muffins",
    price: 9.06,
    img: "https://d1ralsognjng37.cloudfront.net/b9f8427d-bcbe-4014-8b4d-802820f87c7f.jpeg",
  },
  {
    name: "Coffee Roll",
    price: 2.48,
    img: "https://d1ralsognjng37.cloudfront.net/4c2d8732-d9e8-4501-95e5-fdc44b2ba0d0.jpeg",
  },
  {
    name: "Apple Fritter",
    price: 2.48,
    img: "https://d1ralsognjng37.cloudfront.net/0728e119-99bb-477c-b507-13969a54ca99.jpeg",
  },
  {
    name: "Croissant",
    price: 2.11,
    img: "https://d1ralsognjng37.cloudfront.net/f5a43ba7-07ff-42e8-8b06-33dd29b1021e.jpeg",
  },
  {
    name: "English Muffin",
    price: 2.11,
    img: "https://d1ralsognjng37.cloudfront.net/51794699-84fc-4fa5-816f-296c2564a79e.jpeg",
  },
  {
    name: "Hash Browns",
    price: 1.23,
    img: "https://d1ralsognjng37.cloudfront.net/e6f40aaf-5069-4395-8028-1a11d15131ea.jpeg",
  },
];
