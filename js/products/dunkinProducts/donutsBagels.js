export default [
  {
    name: "Donuts",
    price: 1.61,
    img: "https://d1ralsognjng37.cloudfront.net/cbe2e5bc-4db8-4faa-8f33-6436c1087d41.jpeg",
  },
  {
    name: "Assorted 6 Donuts",
    price: 9.61,
    img: "https://d1ralsognjng37.cloudfront.net/d1cf3feb-f09b-4b7d-b54f-93bd1870b8aa.jpeg",
  },
  {
    name: "Assorted 12 Donuts",
    price: 16.86,
    img: "https://d1ralsognjng37.cloudfront.net/77c99490-392e-4f6f-87e2-390a01d08d4e.jpeg",
  },
  {
    name: "Munchkins Donut Hole Treats (5 Un)",
    price: 1.61,
    img: "https://d1ralsognjng37.cloudfront.net/9e47696c-a4dd-4e23-99d7-215b958afc2f.jpeg",
  },
  {
    name: "Munchkins Donut Hole Treats (10 Un)",
    price: 2.5,
    img: "https://d1ralsognjng37.cloudfront.net/9e47696c-a4dd-4e23-99d7-215b958afc2f.jpeg",
  },
  {
    name: "Munchkins Donut Hole Treats (25 Un)",
    price: 9.61,
    img: "https://d1ralsognjng37.cloudfront.net/9e47696c-a4dd-4e23-99d7-215b958afc2f.jpeg",
  },
  {
    name: "Munchkins Donut Hole Treats (50 Un)",
    price: 16.86,
    img: "https://d1ralsognjng37.cloudfront.net/9e47696c-a4dd-4e23-99d7-215b958afc2f.jpeg",
  },
  {
    name: "Bagels",
    price: 2.48,
    img: "https://d1ralsognjng37.cloudfront.net/fc73b034-6b88-4e6f-a6a6-d2745436d9bc.jpeg",
  },
  {
    name: "Bagel with Cream Cheese",
    price: 3.86,
    img: "https://d1ralsognjng37.cloudfront.net/eb63ebc7-deaa-4ae1-8196-61e9516c2bb1.jpeg",
  },
];
