export default [
  {
    name: "Grilled Cheese Melt",
    price: 4.61,
    img: "https://d1ralsognjng37.cloudfront.net/f55764d2-7eff-4a57-8cf2-895d1f0c9a9d.png",
  },
  {
    name: "Grilled Ham & Cheese Melt",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/1f8a45d0-03f5-46c8-ab86-804fcaf6c0f3.png",
  },
  {
    name: "Sweet Black Pepper Bacon Sandwich",
    price: 6.36,
    img: "https://d1ralsognjng37.cloudfront.net/54464c74-2a9d-4b6e-8a25-02794dd2393b.jpeg",
  },
  {
    name: "Wake-Up Wrap Sweet Black Pepper Bacon",
    price: 3.06,
    img: "https://d1ralsognjng37.cloudfront.net/c25c21f2-8a1a-4fcf-9543-8cffccf8db57.jpeg",
  },
  {
    name: "Sourdough Breakfast Sandwich",
    price: 6.36,
    img: "https://d1ralsognjng37.cloudfront.net/69a520ce-8c94-433f-a948-448128e8137e.jpeg",
  },
  {
    name: "Beyond Sausage Sandwich",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/78267c37-e7a9-4f03-8387-d0d48fc40330.jpeg",
  },
  {
    name: "Wake-Up Wrap Beyond Sausage",
    price: 2.23,
    img: "https://d1ralsognjng37.cloudfront.net/a2baef14-2271-4550-933f-4f870bc55dde.jpeg",
  },
  {
    name: "Egg and Cheese",
    price: 4.81,
    img: "https://d1ralsognjng37.cloudfront.net/60718446-6b38-431c-b342-7b38f18d9960.jpeg",
  },
  {
    name: "Sausage Egg and Cheese",
    price: 5.56,
    img: "https://d1ralsognjng37.cloudfront.net/b92312f2-a5dd-4a82-9107-a81879e02358.jpeg",
  },
  {
    name: "Bacon Egg and Cheese",
    price: 5.56,
    img: "https://d1ralsognjng37.cloudfront.net/8d2c46ae-7113-4b70-a99b-d46af1cb53d1.jpeg",
  },
  {
    name: "Turkey Sausage Egg and Cheese",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/a3191120-9397-4d45-bfde-99c4ed3722e7.jpeg",
  },
  {
    name: "Ham Egg and Cheese",
    price: 5.56,
    img: "https://d1ralsognjng37.cloudfront.net/eaefe87e-50ec-43c0-85d6-4eecd4730bae.jpeg",
  },
  {
    name: "Power Breakfast Sandwich",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/3b06c235-7670-4de2-bd0f-8d937405e31a.jpeg",
  },
  {
    name: "Veggie Egg White Omelet",
    price: 4.81,
    img: "https://d1ralsognjng37.cloudfront.net/8d948983-cea7-4307-9805-8cbd67e42975.jpeg",
  },
  {
    name: "Wake-Up Wrap Ham Egg And Cheese",
    price: 2.81,
    img: "https://d1ralsognjng37.cloudfront.net/2eb223ba-36c7-4d3c-ada1-e6f47fb1ba62.jpeg",
  },
  {
    name: "Wake-Up Wrap Sausage Egg And Cheese",
    price: 2.81,
    img: "https://d1ralsognjng37.cloudfront.net/4d8aac33-5ee5-46a9-bf20-edc4361324cd.jpeg",
  },
  {
    name: "Wake-Up Wrap Bacon Egg And Cheese",
    price: 2.81,
    img: "https://d1ralsognjng37.cloudfront.net/037d1bc6-e26f-4724-a4d9-b29507ed9be6.jpeg",
  },
  {
    name: "Wake-Up Wrap Turkey Sausage Egg And Cheese",
    price: 2.81,
    img: "https://d1ralsognjng37.cloudfront.net/4ff26e5f-474b-4da7-994b-27f43b2cbf09.jpeg",
  },
  {
    name: "Wake-Up Wrap Veggie Egg White and Cheese",
    price: 2.31,
    img: "https://d1ralsognjng37.cloudfront.net/0f0f2e85-a1fe-4e2b-a570-2684d28b8a19.jpeg",
  },
  {
    name: "Wake-Up Wrap Egg and Cheese",
    price: 2.31,
    img: "https://d1ralsognjng37.cloudfront.net/ecd61232-dc48-4d20-924c-405d9ff8ba6c.jpeg",
  },
  {
    name: "Hash Browns",
    price: 1.23,
    img: "https://d1ralsognjng37.cloudfront.net/600b1cbc-d951-4642-ba86-551ef3410a45.jpeg",
  },
  {
    name: "Sweet Black Pepper Snackin' Bacon",
    price: 3.11,
    img: "https://d1ralsognjng37.cloudfront.net/4fd95989-1311-4269-9e6b-f762b4059992.jpeg",
  },
];
