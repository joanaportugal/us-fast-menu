export default [
  {
    name: "Bacon, Egg & Cheese 6 Inch with Regular Egg",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/fed4f503-852c-4127-a796-0a4bddc40149.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Footlong with Regular Egg",
    price: 7.29,
    img: "https://d1ralsognjng37.cloudfront.net/fed4f503-852c-4127-a796-0a4bddc40149.jpeg",
  },
  {
    name: "Bacon, Egg & Cheese Wrap",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/e2cd08d7-183a-42b7-9496-3f6c5872aaa2.jpeg",
  },
  {
    name: "Black Forest Ham, Egg & Cheese 6 Inch with Regular Egg",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/e21adfce-f486-4cdb-af2e-93ddffc0b085.jpeg",
  },
  {
    name: "Black Forest Ham, Egg & Cheese Footlong with Regular Egg",
    price: 7.29,
    img: "https://d1ralsognjng37.cloudfront.net/e21adfce-f486-4cdb-af2e-93ddffc0b085.jpeg",
  },
  {
    name: "Black Forest Ham, Egg & Cheese Wrap",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/8ec07b30-d1eb-4c84-8511-26e5d0616323.jpeg",
  },

  {
    name: "Egg & Cheese 6 Inch with Regular Egg",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/74b5ecdc-0c6f-403c-b1a9-eff7310995b9.jpeg",
  },
  {
    name: "Egg & Cheese Footlong with Regular Egg",
    price: 7.29,
    img: "https://d1ralsognjng37.cloudfront.net/74b5ecdc-0c6f-403c-b1a9-eff7310995b9.jpeg",
  },
  {
    name: "Egg & Cheese Wrap",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/7a1f2064-6c8f-42be-b7af-79c4091e0245.jpeg",
  },

  {
    name: "Steak, Egg & Cheese 6 Inch with Regular Egg",
    price: 4.98,
    img: "https://d1ralsognjng37.cloudfront.net/700fc333-b895-45f8-921d-7e966a5a8197.jpeg",
  },
  {
    name: "Steak, Egg & Cheese Footlong with Regular Egg",
    price: 7.29,
    img: "https://d1ralsognjng37.cloudfront.net/700fc333-b895-45f8-921d-7e966a5a8197.jpeg",
  },
  {
    name: "Steak, Egg & Cheese Wrap",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/83b6c9d0-dab8-4b6c-b367-a4b382d94eac.jpeg",
  },
];
