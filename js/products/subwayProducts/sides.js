export default [
  {
    name: "Musselman’s Apple Sauce",
    price: 1.79,
    img: "https://d1ralsognjng37.cloudfront.net/195624d4-4cc7-4339-991d-1ffeb80d1ae3.jpeg",
  },
  {
    name: "Chocolate Chip",
    price: 0.89,
    img: "https://d1ralsognjng37.cloudfront.net/08618511-09f9-4e21-8aba-c1f0e10c6a5c.jpeg",
  },
  {
    name: "Oatmeal Raisin",
    price: 0.89,
    img: "https://d1ralsognjng37.cloudfront.net/51fab756-38b1-4041-92e9-65ad905a5a7e.jpeg",
  },
  {
    name: "White Chip Macadamia Nut",
    price: 0.79,
    img: "https://d1ralsognjng37.cloudfront.net/69dfa884-8b21-467e-943a-c98892bd024e.jpeg",
  },
  {
    name: "Baked Lay's Original",
    price: 1.39,
    img: "https://d1ralsognjng37.cloudfront.net/f949d560-235b-43de-aa36-cef1bea17d52.jpeg",
  },
  {
    name: "DORITOS Nacho Cheese",
    price: 1.19,
    img: "https://d1ralsognjng37.cloudfront.net/93bd8c6c-35fd-4295-9d70-4095b4ae17f1.jpeg",
  },
  {
    name: "SunChips Harvest Cheddar",
    price: 1.29,
    img: "https://d1ralsognjng37.cloudfront.net/93228dda-ad7b-41d3-9134-df060c632261.jpeg",
  },
  {
    name: "Lay's Classic",
    price: 1.39,
    img: "https://d1ralsognjng37.cloudfront.net/1b1cc069-0e4b-45c7-8f68-320932c9240b.jpeg",
  },
  {
    name: "Miss Vickie’s Jalapeño",
    price: 1.39,
    img: "https://d1ralsognjng37.cloudfront.net/f2f98e55-945b-4df9-83b9-7d482cc3a2b5.jpeg",
  },
];
