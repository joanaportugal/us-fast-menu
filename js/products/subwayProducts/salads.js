export default [
  {
    name: "Turkey",
    price: 9.99,
    img: "https://d1ralsognjng37.cloudfront.net/bcfe68d2-317a-430e-8134-2f2ffded70f6.jpeg",
  },
  {
    name: "Steak",
    price: 9.99,
    img: "https://d1ralsognjng37.cloudfront.net/d10afb1b-1a39-48e4-8aa4-a4542c2a78a5.jpeg",
  },
  {
    name: "All-American Club",
    price: 8.99,
    img: "https://d1ralsognjng37.cloudfront.net/b7967b57-302e-4803-a0bb-bb1dd9b03ea2.jpeg",
  },
  {
    name: "B.L.T.",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/9487af82-c91b-4dad-823c-275b9b0cd9f1.jpeg",
  },
  {
    name: "Black Forest Ham",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/f7d11f34-c3e3-44c2-95b1-fa8ba16aed5c.jpeg",
  },
  {
    name: "Buffalo Chicken",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/41ec2e77-bda4-4483-808a-b56d7f376f01.jpeg",
  },
  {
    name: "Chicken & Bacon Ranch",
    price: 8.79,
    img: "https://d1ralsognjng37.cloudfront.net/a32b29e1-f960-44a7-bf48-169e7b445a35.jpeg",
  },
  {
    name: "Cold Cut Combo",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/8f67597c-db9c-4f69-a33e-3d8bc0439af2.jpeg",
  },
  {
    name: "Grilled Chicken",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/41ec2e77-bda4-4483-808a-b56d7f376f01.jpeg",
  },
  {
    name: "Italian B.M.T.",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/e8d0143f-96b9-4188-8ea7-f646b40e038c.jpeg",
  },
  {
    name: "Meatball Marinara",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/4974b74d-66d7-48bd-a58e-8e075dae751f.jpeg",
  },
  {
    name: "Oven Roasted Chicken",
    price: 8.68,
    img: "https://d1ralsognjng37.cloudfront.net/feedcaa4-641d-4ad5-8e44-160cb1c27cd4.jpeg",
  },
  {
    name: "Oven Roasted Turkey",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/0a188e68-8c49-4554-bba0-09174fb2c2a2.jpeg",
  },
  {
    name: "Oven Roasted Turkey & Ham",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/4d750695-678d-43cc-8b19-c80be17452f1.jpeg",
  },
  {
    name: "Spicy Italian",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/ae8c9c81-72c0-483e-9262-f8097e158292.jpeg",
  },
  {
    name: "Steak & Cheese",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/a22be10f-c641-441e-be15-95fa0891c3ac.jpeg",
  },
  {
    name: "Sweet Onion Chicken Teriyaki",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/e6e147b1-39ed-4532-a382-6fce182c1c55.jpeg",
  },
  {
    name: "Tuna",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/ddd401be-3ecd-40f2-9b0c-c68f8ed0d29f.jpeg",
  },
  {
    name: "Veggie Delite",
    price: 7.69,
    img: "https://d1ralsognjng37.cloudfront.net/e389040d-d5a2-49a1-ad52-cd2756906ff7.jpeg",
  },
  {
    name: "Veggie Patty",
    price: 8.89,
    img: "https://d1ralsognjng37.cloudfront.net/9ab67928-3245-44f4-96bd-a23cb9eb9e66.jpeg",
  },
];
