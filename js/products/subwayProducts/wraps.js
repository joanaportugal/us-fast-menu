export default [
  {
    name: "Turkey",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/626772b1-885f-4fbb-9e20-2d82770a5b13.jpeg",
  },
  {
    name: "Steak",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/b1584964-01eb-4a55-8add-3e7a07259381.jpeg",
  },
  {
    name: "B.L.T.",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/c81285d8-6ca8-4171-a570-4ffd41b44e25.jpeg",
  },
  {
    name: "Black Forest Ham",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/9a28cfe2-d4fd-42ab-abcf-4771173d839b.jpeg",
  },
  {
    name: "Buffalo Chicken",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/aa2601a0-d232-47c0-a619-0998affee0b4.jpeg",
  },
  {
    name: "Chicken & Bacon Ranch",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/071f6d37-d6ee-4cec-a4e9-80f19e796956.jpeg",
  },
  {
    name: "Cold Cut Combo",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/b95c1f99-f485-48b1-977d-40f7ad9126db.jpeg",
  },
  {
    name: "Grilled Chicken",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/626772b1-885f-4fbb-9e20-2d82770a5b13.jpeg",
  },
  {
    name: "Italian B.M.T.",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/6b55937a-4300-4134-89c7-40f8af44df82.jpeg",
  },
  {
    name: "Meatball Marinara",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/b1dda34d-404f-4a76-8e75-b88cac37ba07.jpeg",
  },
  {
    name: "Oven Roasted Chicken",
    price: 8.79,
    img: "https://d1ralsognjng37.cloudfront.net/057bf487-50e5-4771-9288-237394cd66e4.jpeg",
  },
  {
    name: "Oven Roasted Turkey",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/0c1e9508-aced-4219-8689-39b57963ead3.jpeg",
  },
  {
    name: "Oven Roasted Turkey & Ham",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/43b7f7f8-1842-4fb8-998c-d9cbf91b1cd5.jpeg",
  },
  {
    name: "Spicy Italian",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/1976c628-5e4c-410d-a86a-29b03ddbe21f.jpeg",
  },
  {
    name: "Steak & Cheese",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/8c78dd9b-c841-4c32-816d-5cc7b54d9915.jpeg",
  },
  {
    name: "Sweet Onion Chicken Teriyaki",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/a2330a53-4ab8-43c1-a5a9-2241e68a9522.jpeg",
  },
  {
    name: "Tuna",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/0b6f34bc-46bc-4936-a83b-5140ee639464.jpeg",
  },
  {
    name: "Veggie Delite",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/5dd88e64-c995-4744-8166-ef2d83f47825.jpeg",
  },
  {
    name: "Veggie Patty",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/846e19fc-f18a-420a-8731-c537a54fb4b9.jpeg",
  },
];
