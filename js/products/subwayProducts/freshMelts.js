export default [
  {
    name: "Ham & Cheese",
    price6Inch: 5.63,
    priceFootlong: 8.81,
    img: "https://d1ralsognjng37.cloudfront.net/85795df3-37a1-4a41-8aed-d033638459c4.jpeg",
  },
  {
    name: "Steak & Cheese",
    price6Inch: 6.89,
    priceFootlong: 10.92,
    img: "https://d1ralsognjng37.cloudfront.net/4074d147-a705-4565-83af-3854b5b0c15e.jpeg",
  },
  {
    name: "Tuna",
    price6Inch: 6.69,
    priceFootlong: 10.22,
    img: "https://d1ralsognjng37.cloudfront.net/8ca3ff38-1dec-4d94-87d9-1895c489587d.jpeg",
  },
  {
    name: "Buffalo Chicken",
    price6Inch: 6.93,
    priceFootlong: 10.22,
    img: "https://d1ralsognjng37.cloudfront.net/af3b9a59-0038-45fd-bf20-e2625849c4a8.jpeg",
  },
  {
    name: "Chicken & Bacon Ranch",
    price6Inch: 7.19,
    priceFootlong: 11.49,
    img: "https://d1ralsognjng37.cloudfront.net/97cf0b25-af3e-43b9-9898-d345e0544e45.jpeg",
  },
  {
    name: "Italian B.M.T.",
    price6Inch: 6.69,
    priceFootlong: 10.22,
    img: "https://d1ralsognjng37.cloudfront.net/f6f71b3a-fcec-4eef-95fa-2f36c63a845d.jpeg",
  },
  {
    name: "Meatball Marinara",
    price6Inch: 5.63,
    priceFootlong: 8.81,
    img: "https://d1ralsognjng37.cloudfront.net/55274b70-af88-4fa1-909a-b28c3226f2c1.jpeg",
  },
  {
    name: "Oven Roasted Turkey",
    price6Inch: 6.69,
    priceFootlong: 10.22,
    img: "https://d1ralsognjng37.cloudfront.net/b14df53a-4e8c-4222-bb19-b66e86445abe.jpeg",
  },
  {
    name: "Spicy Italian",
    price6Inch: 5.51,
    priceFootlong: 8.81,
    img: "https://d1ralsognjng37.cloudfront.net/3da9da3e-f7b9-4305-a0f9-75db8385f293.jpeg",
  },
];
