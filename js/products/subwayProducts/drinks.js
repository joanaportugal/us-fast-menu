export default [
  {
    name: "Coca-Cola",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/c204e39f-de90-4c56-a9d8-cb35a7eac010.jpeg",
  },
  {
    name: "Dasani Water",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/d6e668e0-e29e-48dd-adb1-23f1dec0b2c1.jpeg",
  },
  {
    name: "Diet Coke",
    price: 2.49,
    img: "https://d1ralsognjng37.cloudfront.net/405b81f6-6466-4169-b272-d1686f1e418c.jpeg",
  },
  {
    name: "Honest Kids Super Fruit Punch",
    price: 1.89,
    img: "https://d1ralsognjng37.cloudfront.net/bad8c233-586a-4976-8a92-89f2e3c23c28.jpeg",
  },
  {
    name: "Simply Orange Juice",
    price: 2.19,
    img: "https://d1ralsognjng37.cloudfront.net/4c5f4a97-0ffe-4907-b353-86285f1b4a3d.jpeg",
  },
  {
    name: "Sprite",
    price: 2.49,
    img: "https://d1ralsognjng37.cloudfront.net/ac92e8c0-3092-46f6-8542-825ae3c03f0d.jpeg",
  },
  {
    name: "vitaminwater XXX",
    price: 1.29,
    img: "https://d1ralsognjng37.cloudfront.net/b0a46917-a095-41b7-9802-0dbc9e2add64.jpeg",
  },
];
