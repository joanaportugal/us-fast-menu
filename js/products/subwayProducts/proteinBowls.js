export default [
  {
    name: "Turkey",
    price: 12.99,
    img: "https://d1ralsognjng37.cloudfront.net/449bce08-8efe-40b8-ba82-d24f1d094c04.jpeg",
  },
  {
    name: "Steak",
    price: 14.09,
    img: "https://d1ralsognjng37.cloudfront.net/273b6b2b-4c83-492d-a570-7daced651efd.jpeg",
  },
  {
    name: "All-American Club",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/47dffdbb-8d47-4cbe-acb7-820ce4b3a83a.jpeg",
  },
  {
    name: "B.L.T.",
    price: 8.29,
    img: "https://d1ralsognjng37.cloudfront.net/de4c0f79-07b9-4dd6-aee6-87ab1c4f62cc.jpeg",
  },
  {
    name: "Black Forest Ham",
    price: 8.09,
    img: "https://d1ralsognjng37.cloudfront.net/74cb57e2-5a5e-48f6-a97d-ce4453a076ac.jpeg",
  },
  {
    name: "Buffalo Chicken",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/92c8324f-1145-4124-82a9-a21a95b34ae0.jpeg",
  },
  {
    name: "Chicken & Bacon Ranch",
    price: 11.99,
    img: "https://d1ralsognjng37.cloudfront.net/a32b29e1-f960-44a7-bf48-169e7b445a35.jpeg",
  },
  {
    name: "Cold Cut Combo",
    price: 7.69,
    img: "https://d1ralsognjng37.cloudfront.net/a1225700-ac53-4017-9372-2fb85f6c3971.jpeg",
  },
  {
    name: "Grilled Chicken",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/92c8324f-1145-4124-82a9-a21a95b34ae0.jpeg",
  },
  {
    name: "Italian B.M.T.",
    price: 9.39,
    img: "https://d1ralsognjng37.cloudfront.net/c7b196cf-151d-438c-8c72-f67761c69232.jpeg",
  },
  {
    name: "Meatball Marinara",
    price: 8.09,
    img: "https://d1ralsognjng37.cloudfront.net/6f7206db-064b-4067-895b-3e735dfcf545.jpeg",
  },
  {
    name: "Oven Roasted Chicken",
    price: 9.99,
    img: "https://d1ralsognjng37.cloudfront.net/d69432a1-6554-439b-9dd5-cec7ecce9208.jpeg",
  },
  {
    name: "Oven Roasted Turkey",
    price: 9.39,
    img: "https://d1ralsognjng37.cloudfront.net/ee499ea2-05d5-4161-945e-5ddc38ca53ba.jpeg",
  },
  {
    name: "Oven Roasted Turkey & Ham",
    price: 9.09,
    img: "https://d1ralsognjng37.cloudfront.net/23f52bda-d011-4743-b2bd-e528a638d9c4.jpeg",
  },
  {
    name: "Spicy Italian",
    price: 8.09,
    img: "https://d1ralsognjng37.cloudfront.net/6b117fb3-6847-4b4b-a22a-46a2fa1f470f.jpeg",
  },
  {
    name: "Steak & Cheese",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/a11e66b1-db2d-4ceb-94a0-d37baf97f11f.jpeg",
  },
  {
    name: "Sweet Onion Chicken Teriyaki",
    price: 9.39,
    img: "https://d1ralsognjng37.cloudfront.net/1c87bcdc-7b19-4a93-9f34-278a5b4b57a1.jpeg",
  },
  {
    name: "Tuna",
    price: 9.39,
    img: "https://d1ralsognjng37.cloudfront.net/0fc3c98a-4b91-43e6-ae92-589bad790731.jpeg",
  },
  {
    name: "Veggie Patty",
    price: 10.29,
    img: "https://d1ralsognjng37.cloudfront.net/d6c76e3d-4379-423c-9802-4f72983d52f5.jpeg",
  },
];
