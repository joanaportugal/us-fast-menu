import featured from "./tacobellProducts/featured.js";
import combos from "./tacobellProducts/combos.js";
import groups from "./tacobellProducts/groups.js";
import specialties from "./tacobellProducts/specialties.js";
import tacos from "./tacobellProducts/tacos.js";
import burritos from "./tacobellProducts/burritos.js";
import quesadillas from "./tacobellProducts/quesadillas.js";
import nachos from "./tacobellProducts/nachos.js";
import valueMenu from "./tacobellProducts/valueMenu.js";
import sides from "./tacobellProducts/sides.js";
import drinks from "./tacobellProducts/drinks.js";
import bowls from "./tacobellProducts/bowls.js";
import veggie from "./tacobellProducts/veggie.js";

export default {
  featured,
  combos,
  groups,
  specialties,
  tacos,
  burritos,
  quesadillas,
  nachos,
  valueMenu,
  sides,
  drinks,
  bowls,
  veggie,
};
