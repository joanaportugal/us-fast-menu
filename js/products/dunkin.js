import espressoDrinks from "./dunkinProducts/espressoDrinks.js";
import icedDrinks from "./dunkinProducts/icedDrinks.js";
import hotDrinks from "./dunkinProducts/hotDrinks.js";
import frozenDrinks from "./dunkinProducts/frozenDrinks.js";
import sandwichesWraps from "./dunkinProducts/sandwichesWraps.js";
import donutsBagels from "./dunkinProducts/donutsBagels.js";
import bakerySnacks from "./dunkinProducts/bakerySnacks.js";

export default {
  espressoDrinks,
  icedDrinks,
  hotDrinks,
  frozenDrinks,
  sandwichesWraps,
  donutsBagels,
  bakerySnacks,
};
