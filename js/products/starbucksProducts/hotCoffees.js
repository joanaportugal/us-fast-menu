export default [
  {
    name: "Caffè Americano",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/7c4f194a-b5d0-4a8c-a586-d44ecdd674e2.jpeg",
  },
  {
    name: "Blonde Roast",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/b58bc246-baaf-4945-872d-b830e869d4e6.jpeg",
  },
  {
    name: "Caffè Misto",
    price: 3.45,
    img: "https://d1ralsognjng37.cloudfront.net/cb29fe6e-ab29-4c74-9adc-2df2b3274b4a.jpeg",
  },
  {
    name: "Featured Starbucks Dark Roast Coffee",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/f4ca2f97-e8e6-4fa1-9b4f-b817cd6f97b6.jpeg",
  },
  {
    name: "Pike Place Roast",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/54d68362-554a-421d-99ae-45e562fc3526.jpeg",
  },
  {
    name: "Decaf Pike Place Roast",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/916147fa-73bf-483b-a7c4-54c1e82b5d7c.jpeg",
  },
  {
    name: "Cappuccino",
    price: 4.25,
    img: "https://d1ralsognjng37.cloudfront.net/213a7859-5624-476c-91ae-724b2aa0de8a.jpeg",
  },
  {
    name: "Flat White",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/1d3f542a-aabd-4fe0-939f-e8ac39cf0b64.jpeg",
  },
  {
    name: "Honey Almondmilk Flat White",
    price: 5.65,
    img: "https://d1ralsognjng37.cloudfront.net/248f2036-5bd7-4c8a-835a-b87987580c36.jpeg",
  },
  {
    name: "Caffè Latte",
    price: 4.25,
    img: "https://d1ralsognjng37.cloudfront.net/494ad2a3-e9a8-4033-b6e0-3fb9eb3992cc.jpeg",
  },
  {
    name: "Cinnamon Dolce Latte",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/bd968346-f80d-4ba8-8ba0-35def332fb41.jpeg",
  },
  {
    name: "Starbucks Blonde Vanilla Latte",
    price: 4.45,
    img: "https://d1ralsognjng37.cloudfront.net/24fa9867-6c37-42e0-a2d6-52522802bff6.jpeg",
  },
  {
    name: "Espresso Macchiato",
    price: 2.95,
    img: "https://d1ralsognjng37.cloudfront.net/fc8d9076-a8c2-4ecd-8fae-cb1e9bd223ae.jpeg",
  },
  {
    name: "Caramel Macchiato",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/35f51933-2cb0-4bc1-a264-469e8b37a302.jpeg",
  },
  {
    name: "Caffè Mocha",
    price: 4.85,
    img: "https://d1ralsognjng37.cloudfront.net/b4317f47-d66e-4ccb-8c12-e7e1b9bcddb7.jpeg",
  },
  {
    name: "White Chocolate Mocha",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/bbbbf220-88eb-4ab4-b756-49ef4c1f1096.jpeg",
  },
];
