export default [
  {
    name: "White Hot Chocolate",
    pric: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/196522f6-9d10-4097-afc9-58418b2f6a52.jpeg",
  },
  {
    name: "Hot Chocolate",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/ee178b53-cf3d-49ab-9f0f-cf61fcdd36a0.jpeg",
  },
  {
    name: "Caramel Apple Spice",
    pric: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/c9b88bdd-9ae1-49ec-8bbd-8a899d77244c.jpeg",
  },
  {
    name: "Steamed Apple Juice",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/0561aee7-7c41-43e6-8a34-2aedf9f42a13.jpeg",
  },
  {
    name: "Cinnamon Dolce Crème",
    pric: 4.55,
    img: "https://d1ralsognjng37.cloudfront.net/713644bc-d7af-4407-a3b2-99bb3d608b83.jpeg",
  },
  {
    name: "Steamed Milk",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/0c2d455e-405c-41bd-bcf3-23c1ac18c30e.jpeg",
  },
  {
    name: "Vanilla Crème",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/c28902e5-a8ef-4d35-8d12-cb6fa6b0144d.jpeg",
  },
];
