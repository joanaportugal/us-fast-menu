export default [
  {
    name: "Everything Bagel",
    price: 2.35,
    img: "https://d1ralsognjng37.cloudfront.net/093c1369-6c10-4c3b-b987-1b3cfe6935a6.jpeg",
  },
  {
    name: "Cinnamon Raisin Bagel",
    price: 2.35,
    img: "https://d1ralsognjng37.cloudfront.net/75464e68-3afe-4085-a5df-82c60e003202.jpeg",
  },
  {
    name: "Avocado Spread",
    price: 1.45,
    img: "https://d1ralsognjng37.cloudfront.net/ec2b61c7-52b4-4cb6-92fe-336cbd2a5993.jpeg",
  },
  {
    name: "Plain Bagel",
    price: 2.35,
    img: "https://d1ralsognjng37.cloudfront.net/74569f47-5ef3-4360-a226-446828f34e60.jpeg",
  },
  {
    name: "Chocolate Cake Pop",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/6ec5794d-234f-4c42-a3ce-b404cf70b0e4.jpeg",
  },
  {
    name: "Double Chocolate Brownie",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/aba01177-1429-4478-b926-a3b0fa44b49b.jpeg",
  },
  {
    name: "Chocolate Chip Cookie",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/a6c5bd48-48c7-4fab-aeb8-b4d8f9fa4642.jpeg",
  },
  {
    name: "Marshmallow Dream Bar",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/2266de55-6af3-4c30-b565-7b06254eaa1b.jpeg",
  },
  {
    name: "Butter Croissant",
    price: 3.45,
    img: "https://d1ralsognjng37.cloudfront.net/d29cacd9-bff5-4f37-9810-645e77f00a82.jpeg",
  },
  {
    name: "Almond Croissant",
    price: 4.35,
    img: "https://d1ralsognjng37.cloudfront.net/8c248a7b-5670-415b-901b-0a1adc33cc0b.jpeg",
  },
  {
    name: "Cinnamon Coffee Cake",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/02a1f317-53c5-40b3-a8b9-3165e6a07a2f.jpeg",
  },
  {
    name: "Iced Lemon Loaf",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/21572aae-fcbd-4353-8e62-e4e8192505d5.jpeg",
  },
  {
    name: "Pumpkin Loaf",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/44dafe6b-97ec-4e2b-b8d0-820b551ea27f.jpeg",
  },
  {
    name: "Banana Nut Loaf",
    price: 3.75,
    img: "https://d1ralsognjng37.cloudfront.net/f0516976-c1aa-4bcb-b4bb-39e5b7c79b1a.jpeg",
  },
  {
    name: "Glazed Doughnut",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/f417dd90-0b20-4e0a-b59b-9fee8fd09fe1.jpeg",
  },
  {
    name: "Cheese Danish",
    price: 3.45,
    img: "https://d1ralsognjng37.cloudfront.net/2d89ed15-118f-44a1-8b9b-213d839b0385.jpeg",
  },
  {
    name: "Blueberry Muffin",
    price: 3.15,
    img: "https://d1ralsognjng37.cloudfront.net/e709aca0-f3f2-463f-a09b-e73bd1e79800.jpeg",
  },
  {
    name: "Blueberry Scone",
    price: 3.45,
    img: "https://d1ralsognjng37.cloudfront.net/d2350e40-75cf-494f-a996-5d5b226a4585.jpeg",
  },
  {
    name: "Petite Vanilla Bean Scone",
    price: 1.65,
    img: "https://d1ralsognjng37.cloudfront.net/cc213fdc-ccc9-4b4e-8b50-2c799bb030cf.jpeg",
  },
];
