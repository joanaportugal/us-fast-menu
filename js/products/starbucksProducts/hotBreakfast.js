export default [
  {
    name: "Bacon, Gouda & Egg Sandwich",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/a058bf9e-2767-4f24-9555-e94f943da30f.jpeg",
  },
  {
    name: "Turkey Bacon, Cheddar & Egg White Sandwich",
    price: 4.85,
    img: "https://d1ralsognjng37.cloudfront.net/b1e5c8e5-c16f-4b93-98dd-69aa6fcdc2c8.jpeg",
  },
  {
    name: "Spinach, Feta & Egg White Wrap",
    price: 4.95,
    img: "https://d1ralsognjng37.cloudfront.net/9d6dd633-2d02-4890-bda3-432827aed43a.jpeg",
  },
  {
    name: "Double-Smoked Bacon, Cheddar & Egg Sandwich",
    price: 5.95,
    img: "https://d1ralsognjng37.cloudfront.net/d6c2f154-d363-4b6b-910f-9b3967805165.jpeg",
  },
  {
    name: "Sausage, Cheddar & Egg Sandwich",
    price: 4.55,
    img: "https://d1ralsognjng37.cloudfront.net/4388a5b6-629b-48c7-9628-0b78f53e3ce4.jpeg",
  },
  {
    name: "Avocado Spread",
    price: 1.45,
    img: "https://d1ralsognjng37.cloudfront.net/ec2b61c7-52b4-4cb6-92fe-336cbd2a5993.jpeg",
  },
  {
    name: "Impossible Breakfast Sandwich",
    price: 5.95,
    img: "https://d1ralsognjng37.cloudfront.net/a4823c95-d17d-48cc-98ee-749824006b9a.jpeg",
  },
  {
    name: "Kale & Portabella Mushroom Sous Vide Egg Bites",
    price: 5.75,
    img: "https://d1ralsognjng37.cloudfront.net/50d32bf4-986d-4306-ae1e-8de2783bf7d8.jpeg",
  },
];
