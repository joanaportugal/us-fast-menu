export default [
  {
    name: "Chicken & Bacon Panini",
    price: 7.75,
    img: "https://d1ralsognjng37.cloudfront.net/538b2bc8-dc4b-4e1c-b764-60e1708375e6.jpeg",
  },
  {
    name: "Tomato & Mozzarella Panini",
    price: 7.25,
    img: "https://d1ralsognjng37.cloudfront.net/16ef684f-69ad-483f-9872-048b419a6ffe.jpeg",
  },
  {
    name: "Chicken Caprese Panini",
    price: 7.75,
    img: "https://d1ralsognjng37.cloudfront.net/8a19ed05-393c-4c60-9ec4-88af817918ea.jpeg",
  },
  {
    name: "Avocado Spread",
    price: 1.45,
    img: "https://d1ralsognjng37.cloudfront.net/ec2b61c7-52b4-4cb6-92fe-336cbd2a5993.jpeg",
  },
  {
    name: "Turkey & Pesto Panini",
    price: 7.45,
    img: "https://d1ralsognjng37.cloudfront.net/44ebf1c7-8f75-4962-a8f0-95dcb33cb3b4.jpeg",
  },
  {
    name: "Ham & Swiss Panini",
    price: 7.75,
    img: "https://d1ralsognjng37.cloudfront.net/4012cbaa-1935-4309-8474-4c2295b24ec3.jpeg",
  },
  {
    name: "Eggs & Cheddar Protein Box",
    price: 7.25,
    img: "https://d1ralsognjng37.cloudfront.net/9e241537-f05c-4f85-9397-d270c124987b.jpeg",
  },
  {
    name: "Cheese & Fruit Protein Box",
    price: 6.85,
    img: "https://d1ralsognjng37.cloudfront.net/6a799723-ad70-4c60-b535-ead8e1bfee87.jpeg",
  },
  {
    name: "Grilled Chicken and Hummus Protein Box",
    price: 7.45,
    img: "https://d1ralsognjng37.cloudfront.net/53dc13b1-b441-4774-b394-3f0a022ebc19.jpeg",
  },
  {
    name: "Chicken & Quinoa Protein Bowl with Black Beans and Greens",
    price: 9.75,
    img: "https://d1ralsognjng37.cloudfront.net/43685bea-e872-4bfe-a440-d6c3125fa7bc.jpeg",
  },
];
