export default [
  {
    name: "Vanilla Biscotti with Almonds",
    price: 1.95,
    img: "https://d1ralsognjng37.cloudfront.net/1b908c1a-32bd-44a2-9745-4d3bc54eda9c.jpeg",
  },
  {
    name: "Dark Chocolate Grahams (2-Pack)",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/fdc390f7-cff3-4a61-aeac-9d4eca58c16a.jpeg",
  },
  {
    name: "Rip van Wafels – Honey & Oats",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/7c054d0c-d790-4283-a33d-187193a57ab9.jpeg",
  },
  {
    name: "Chocolate Covered Espresso Beans",
    price: 3.15,
    img: "https://d1ralsognjng37.cloudfront.net/810d67fa-f5ea-4696-8332-888ac38ede8a.jpeg",
  },
  {
    name: "Salted Almond Chocolate Bites",
    price: 3.15,
    img: "https://d1ralsognjng37.cloudfront.net/fbef1108-3142-43d0-9dea-59a0f1045442.jpeg",
  },
  {
    name: "Squirrel Brand – Fruit & Nut",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/973420ea-1129-4de6-bbed-7727a031765b.jpeg",
  },
  {
    name: "Squirrel Brand – Classic Almonds",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/571b41fa-6a13-4e30-8606-7860610d2f73.jpeg",
  },
  {
    name: "Peter Rabbit Organics – Apple & Grape",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/6336a836-1ac2-4402-9f7f-3c49bfe83ffe.jpeg",
  },
  {
    name: "Peter Rabbit Organics – Strawberry Banana",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/917b6916-2883-461d-94e9-59db5600a7cc.jpeg",
  },
  {
    name: "Butter Gourmet Popcorn",
    price: 1.75,
    img: "https://d1ralsognjng37.cloudfront.net/d144139d-fa42-48c1-aaf0-23602321c16b.jpeg",
  },
  {
    name: "Starbucks BBQ Potato Chips",
    price: 1.95,
    img: "https://d1ralsognjng37.cloudfront.net/46798bbb-b1d0-4a01-8769-a0ad6ddc6984.jpeg",
  },
  {
    name: "Simply Salted Kettle Potato Chips",
    price: 1.95,
    img: "https://d1ralsognjng37.cloudfront.net/207db9e9-5171-4f26-b7ee-b0019bdd56eb.jpeg",
  },
  {
    name: "Hippeas – White Cheddar",
    price: 2.25,
    img: "https://d1ralsognjng37.cloudfront.net/2b434770-ba57-4a05-b16e-be7fccd23bdd.jpeg",
  },
  {
    name: "Salt & Vinegar Kettle Potato Chips",
    price: 1.95,
    img: "https://d1ralsognjng37.cloudfront.net/747b7fe6-c8ed-47ce-a301-45ef9f143419.jpeg",
  },
  {
    name: "Sweet Potato Kettle Potato Chips",
    price: 1.95,
    img: "https://d1ralsognjng37.cloudfront.net/28db6bec-8af1-4982-9115-3dce1779e524.jpeg",
  },
  {
    name: "String Cheese",
    price: 1.45,
    img: "https://d1ralsognjng37.cloudfront.net/08b651c0-4816-423f-99a2-202f06ddd588.jpeg",
  },
  {
    name: "Country Archer – Hickory Smoked Turkey Jerkey",
    price: 7.95,
    img: "https://d1ralsognjng37.cloudfront.net/f157357d-504f-4c0c-b4df-c58564bd03c7.jpeg",
  },
  {
    name: "Creminelli Snack Tray – Sopressata Salami & Monterey Jack",
    price: 7.45,
    img: "https://d1ralsognjng37.cloudfront.net/7470f0df-f1f7-43ef-aef4-11eda0458f66.jpeg",
  },
  {
    name: "This Bar Saves Lives – Dark Chocolate Peanut Butter & Sea Salt Bar",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/becbed19-950d-4a3f-b8a2-444b7c0a839b.jpeg",
  },
  {
    name: "This Bar Saves Lives – Madagascar Vanilla Almond & Honey Bar",
    price: 2.85,
    img: "https://d1ralsognjng37.cloudfront.net/07e9d79b-033d-4b2f-a98d-2248d5f088b1.jpeg",
  },
  {
    name: "KIND – Blueberry Vanilla & Cashew Bar",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/2ff785af-9e1e-4947-b33c-08feae141a9e.jpeg",
  },
  {
    name: "KIND – Salted Caramel & Dark Chocolate Nut Bar",
    price: 2.65,
    img: "https://d1ralsognjng37.cloudfront.net/5a6a2a21-d709-4918-b1a9-ae9c71072b6d.jpeg",
  },
  {
    name: "Perfect Bar – Peanut Butter",
    price: 4.35,
    img: "https://d1ralsognjng37.cloudfront.net/f3d35c30-0d83-4d22-b9d6-dc0f124ddb8c.jpeg",
  },
  {
    name: "Perfect Bar – Dark Chocolate Chip Peanut Butter",
    price: 4.35,
    img: "https://d1ralsognjng37.cloudfront.net/bd2f6790-f335-4f03-acb1-f1ec36e8707b.jpeg",
  },
  {
    name: "Avocado Spread",
    price: 1.45,
    img: "https://d1ralsognjng37.cloudfront.net/ec2b61c7-52b4-4cb6-92fe-336cbd2a5993.jpeg",
  },
];
