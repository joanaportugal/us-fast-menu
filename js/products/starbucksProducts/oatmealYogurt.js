export default [
  {
    name: "Siggi's Yogurt Cup 0% Vanilla",
    price: 3.15,
    img: "https://d1ralsognjng37.cloudfront.net/5227cf96-fdc1-4019-86b7-9dc25357f888.jpeg",
  },
  {
    name: "Strawberry Overnight Grains",
    price: 4.35,
    img: "https://d1ralsognjng37.cloudfront.net/412a17c9-302a-418a-8fa3-110526ad0f43.jpeg",
  },
  {
    name: "Berry Trio Parfait",
    price: 4.35,
    img: "https://d1ralsognjng37.cloudfront.net/dba17ada-aea3-438c-b457-af071a2a918a.jpeg",
  },
  {
    name: "Classic Oatmeal",
    price: 3.95,
    img: "https://d1ralsognjng37.cloudfront.net/ee3af9f5-42c8-4da5-b3f5-e143984be1a3.jpeg",
  },
];
