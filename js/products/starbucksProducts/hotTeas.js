export default [
  {
    name: "Chai Tea Latte",
    price: 4.85,
    img: "https://d1ralsognjng37.cloudfront.net/59e31377-3cf1-411e-8977-69faa5ec6714.jpeg",
  },
  {
    name: "Matcha Tea Latte",
    price: 4.85,
    img: "https://d1ralsognjng37.cloudfront.net/08a3561d-11e0-445d-98ca-6ef71ab4c174.jpeg",
  },
];
