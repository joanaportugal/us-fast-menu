export default [
  {
    name: "3 pc. Chicken Box",
    price: 19.19,
    img: "https://d1ralsognjng37.cloudfront.net/005dc54a-1065-48ca-8aac-34431d5574b9.jpeg",
  },
  {
    name: "5 pc. Chicken Box",
    price: 19.19,
    img: "https://d1ralsognjng37.cloudfront.net/412687d1-aaae-4969-b2a3-dddf7a25ab42.jpeg",
  },
  {
    name: "Classic Chicken Sandwich Box",
    price: 19.19,
    img: "https://d1ralsognjng37.cloudfront.net/5814336d-153e-47fe-823b-e29e59d53132.png",
  },
  {
    name: "Spicy Chicken Sandwich Box",
    price: 19.19,
    img: "https://d1ralsognjng37.cloudfront.net/30daf327-1298-416f-aa06-9911c107c7c1.png",
  },
];
