export default [
  {
    name: "1 pc. Breast Fill Up",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/98e6bd33-355c-4a66-9158-ed1364be5dcf.jpeg",
  },
  {
    name: "3 pc. Crispy Tenders Fill Up",
    price: 13.79,
    img: "https://d1ralsognjng37.cloudfront.net/c46d4ae7-4ea0-449d-80e1-df447b6c15a9.jpeg",
  },
  {
    name: "Famous Bowl Fill Up",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/36a698f8-9312-499e-8384-35679f8c549d.jpeg",
  },
  {
    name: "Pot Pie Fill Up",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/31915e4d-b157-4a88-8df1-2352164959e1.jpeg",
  },
];
