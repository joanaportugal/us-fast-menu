export default [
  {
    name: "Secret Recipe Fries",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/5af2c34b-5861-49c8-9494-f024bb680198.jpeg",
  },
  {
    name: "Mashed Potatoes & Gravy",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/e3b3ff98-68c2-49bf-af26-ff7deccead5d.jpeg",
  },
  {
    name: "Mashed Potatoes (No Gravy)",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/3e2a2826-f571-434d-a210-4d3fd0e94476.jpeg",
  },
  {
    name: "Cole Slaw",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/88aef80b-99d2-49bb-b3fa-4fc5f06b14e2.jpeg",
  },
  {
    name: "Whole Kernel Corn",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/fb001d3b-6b9a-4c0b-b25e-cdd40d23c4f0.jpeg",
  },
  {
    name: "Mac & Cheese",
    price: 3.95,
    priceLarge: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/139fc606-52b0-40ae-aa24-72c88505b417.jpeg",
  },
  {
    name: "Gravy",
    price: 2.99,
    priceLarge: 5.39,
    img: "https://d1ralsognjng37.cloudfront.net/643427d6-425d-4bc7-8b63-316d2a39333b.jpeg",
  },
  {
    name: "2 Biscuits",
    price: 3.95,
    img: "https://d1ralsognjng37.cloudfront.net/d15df3f2-a78f-4ded-90a0-55c2072644d6.png",
  },
];
