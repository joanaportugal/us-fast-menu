export default [
  {
    name: "Medium Beverage",
    price: 3.59,
    img: "https://d1ralsognjng37.cloudfront.net/7195b5dd-e306-4733-896a-5e952aac286f.jpeg",
  },
  {
    name: "Large Beverage",
    price: 3.83,
    img: "https://d1ralsognjng37.cloudfront.net/7195b5dd-e306-4733-896a-5e952aac286f.jpeg",
  },
  {
    name: "1/2 Gallon Beverage Bucket",
    price: 7.19,
    img: "https://d1ralsognjng37.cloudfront.net/2f3d0125-710d-4e87-a5e0-dbeec213f748.jpeg",
  },
];
