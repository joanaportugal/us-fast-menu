export default [
  {
    name: "8 pc. Chicken",
    price: 28.79,
    img: "https://d1ralsognjng37.cloudfront.net/f6972950-7c74-4829-ad93-89c546d5212a.jpeg",
  },
  {
    name: "12 pc. Chicken",
    price: 39.59,
    img: "https://d1ralsognjng37.cloudfront.net/f6972950-7c74-4829-ad93-89c546d5212a.jpeg",
  },
  {
    name: "16 pc. Chicken",
    price: 45.59,
    img: "https://d1ralsognjng37.cloudfront.net/f6972950-7c74-4829-ad93-89c546d5212a.jpeg",
  },
  {
    name: "12 Crispy Chicken Tenders",
    price: 28.79,
    img: "https://d1ralsognjng37.cloudfront.net/d402b337-d06e-4244-921f-a8fafc79f267.jpeg",
  },
  {
    name: "Crispy Tenders Share Meal",
    price: 21.59,
    img: "https://d1ralsognjng37.cloudfront.net/813a3009-e634-4e23-9456-99031e20948a.jpeg",
  },
];
