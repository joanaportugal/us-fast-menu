export default [
  {
    name: "A La Carte Crispy Tender",
    price: 2.99,
    img: "https://d1ralsognjng37.cloudfront.net/749f3d48-8aa3-49b0-888d-3ab6fa8f1f75.jpeg",
  },
  {
    name: "A La Carte Breast",
    price: 4.79,
    img: "https://d1ralsognjng37.cloudfront.net/53fbf055-c200-40e5-b240-ad57b59f9baa.png",
  },
  {
    name: "A La Carte Drum",
    price: 2.63,
    img: "https://d1ralsognjng37.cloudfront.net/a34a2080-0e6d-44f5-9eab-8e21612cfc0b.png",
  },
  {
    name: "A La Carte Thigh",
    price: 3.47,
    img: "https://d1ralsognjng37.cloudfront.net/cc63d36c-f690-40da-bba6-6316da0c5541.jpeg",
  },
  {
    name: "A La Carte Wing",
    price: 2.39,
    img: "https://d1ralsognjng37.cloudfront.net/0d072f9d-8af9-40f5-bc89-84465d34353e.jpeg",
  },
  {
    name: "Large Popcorn Nuggets",
    price: 6.59,
    img: "https://d1ralsognjng37.cloudfront.net/0f654063-2ac3-4d40-a890-d6de09aa5657.jpeg",
  },
];
