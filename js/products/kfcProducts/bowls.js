export default [
  {
    name: "Famous Bowl",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/3a0fc359-4a46-4c58-bfb9-786b1cddd63b.jpeg",
  },
  {
    name: "Spicy Famous Bowl",
    price: 8.63,
    img: "https://d1ralsognjng37.cloudfront.net/85e83e7b-2321-4832-a667-49ecea0deecc.jpeg",
  },
  {
    name: "Spicy Famous Bowl Combo",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/32df8979-7b4f-43eb-87b7-a6cab4b14a37.jpeg",
  },
];
