export default [
  {
    name: "8 pc. Family Fill Up Bucket Meal",
    price: 41.99,
    img: "https://d1ralsognjng37.cloudfront.net/b5c6434d-4675-4e2a-ba11-4d04147c3d65.jpeg",
  },
  {
    name: "8 pc. Family Bucket Meal",
    price: 41.99,
    img: "https://d1ralsognjng37.cloudfront.net/b5c6434d-4675-4e2a-ba11-4d04147c3d65.jpeg",
  },
  {
    name: "12 pc. Family Bucket Meal",
    price: 55.19,
    img: "https://d1ralsognjng37.cloudfront.net/b5c6434d-4675-4e2a-ba11-4d04147c3d65.jpeg",
  },
  {
    name: "16 pc. Family Bucket Meal",
    price: 68.39,
    img: "https://d1ralsognjng37.cloudfront.net/b5c6434d-4675-4e2a-ba11-4d04147c3d65.jpeg",
  },
  {
    name: "10 Piece Feast",
    price: 53.99,
    img: "https://d1ralsognjng37.cloudfront.net/0397e771-efbd-4764-b645-85a705aba412.jpeg",
  },
  {
    name: "12 Tenders Family Bucket Meal",
    price: 43.19,
    img: "https://d1ralsognjng37.cloudfront.net/fe21b6cd-e53d-4c19-8b4c-a3fd670eadb8.jpeg",
  },
];
