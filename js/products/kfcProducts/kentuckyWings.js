export default [
  {
    name: "6 Kentucky Fried Wings",
    price: 9.59,
    img: "https://d1ralsognjng37.cloudfront.net/8cef7b22-4b74-4dff-9610-5de252c29806.jpeg",
  },
  {
    name: "12 Kentucky Fried Wings",
    price: 18.59,
    img: "https://d1ralsognjng37.cloudfront.net/34a96b2c-35bd-41d3-a103-65f6c2dfe921.jpeg",
  },
  {
    name: "24 Kentucky Fried Wings",
    price: 37.19,
    img: "https://d1ralsognjng37.cloudfront.net/ecd56776-13e0-4991-9002-dd526fc4a2e1.jpeg",
  },
  {
    name: "48 Kentucky Fried Wings",
    price: 73.19,
    img: "https://d1ralsognjng37.cloudfront.net/fd34f006-5399-4d73-9e4e-73e62136fd85.png",
  },
];
