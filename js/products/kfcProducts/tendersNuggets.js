export default [
  {
    name: "Crispy Tenders Combo",
    price: 16.19,
    img: "https://d1ralsognjng37.cloudfront.net/e8c91fe4-2e15-4fed-beaa-b927292b6b21.jpeg",
  },
  {
    name: "Popcorn Nuggets Combo",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/2d2a7a43-9786-434e-88d7-e10d7fbcf1d2.png",
  },
  {
    name: "Nashville Hot Crispy Tenders Combo",
    price: 14.39,
    img: "https://d1ralsognjng37.cloudfront.net/dc0a7780-d27f-4218-a7f2-210ac9b2e7b7.jpeg",
  },
];
