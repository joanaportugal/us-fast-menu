export default [
  {
    name: "Classic Chicken Sandwich",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/33fe4ef1-e8fa-4da9-bd4f-290560cf7323.png",
  },
  {
    name: "Classic Chicken Sandwich Combo",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/4cea8c90-899c-407d-831b-8b98582af0cf.png",
  },
  {
    name: "Spicy Chicken Sandwich",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/4813be5b-5235-446e-b8de-17ccf1666e40.jpeg",
  },
  {
    name: "Spicy Chicken Sandwich Combo",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/c28e9d08-8964-476a-bcde-76c7d212abae.png",
  },
  {
    name: "Chicken Little",
    price: 8.39,
    img: "https://d1ralsognjng37.cloudfront.net/7e7a23f3-2217-4d75-b757-4c52b61f469e.jpeg",
  },
  {
    name: "Chicken Little Combo",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/2979f098-bac2-444e-82fa-c2115dc02597.jpeg",
  },
];
