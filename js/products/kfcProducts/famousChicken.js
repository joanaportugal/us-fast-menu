export default [
  {
    name: "2 pc. Drum & Thigh Fill Up",
    price: 13.19,
    img: "https://d1ralsognjng37.cloudfront.net/35c73a19-96d3-4115-9cdb-9b58120f516e.jpeg",
  },
  {
    name: "2 pc. Breast & Wing Combo",
    price: 14.99,
    img: "https://d1ralsognjng37.cloudfront.net/ad5e72dd-e15a-4b54-be81-97ca7230f836.jpeg",
  },
  {
    name: "3 pc. Chicken Combo",
    price: 16.79,
    img: "https://d1ralsognjng37.cloudfront.net/d1115f5f-1b54-4d6a-831c-74308c9fdef6.jpeg",
  },
  {
    name: "4 pc. Chicken Combo",
    price: 19.19,
    img: "https://d1ralsognjng37.cloudfront.net/9a42f65a-435d-4617-91ef-56be921b45db.jpeg",
  },
];
